﻿using OrderToAx.Order.Controllers;
using OrderToAx.Order.Table;
using PrintBarcode;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace OrderToAx.Order
{

    class PrintQueryClass
    {
        //OK ค้นหาสิทธิ์เครื่อง ว่ามีการตั้งค่าไว้หรือยัง
        internal static string GetDepByComName(string comName)
        {
            return Controllers.ConnectionsDB.GetSelectSQL($@"
                SELECT  DEPT_ID	FROM    SHOP_BRANCH_PERMISSION  WITH(NOLOCK) 
                WHERE   COM_NAME = '{comName}'
                        AND SHOP_BRANCH_PERMISSION.SHOW_ID = 'A05'
            ").Rows[0]["DEPT_ID"].ToString();
        }
        //OK Update การใช้งานโปรแกรมล่าสุด
        internal static void UpdateUse()
        {
            string sqlUpMachine = $@"
                    UPDATE  SHOP_BRANCH_PERMISSION 
                    SET     DATEUPD = GETDATE(),
                            REMARK = '{DateTime.Now}',
                            WHONAMEUPD = '{Environment.UserName}',
                            VERSIONUPD = '{Controllers.Connections.SystemVersion}' 
                    WHERE   COM_NAME = '{Connections.SystemComName}' AND SHOW_ID = 'A05'  ";
            ConnectionsDB.ExecuteSQL_Main(sqlUpMachine);
        }
        //OK ค้นหากลุ่มสินค้าตามแผนก
        internal static DataTable GetGroupItem(string _Dept)
        {
            string sql = $@"
                    SELECT  ITEMGROUPID,ITEMGROUPID + ' : ' + ITEMGROUPNAME AS ITEMGROUPNAME
                    FROM    SHOP_ITEMGROUP WITH (NOLOCK)
                    WHERE   ITEMGROUPDEPT = '{_Dept}' ";
            return Controllers.ConnectionsDB.GetSelectSQL(sql);
        }

        #region AndroidOrder
        //ค้นหาเอกสารทั้งหมด สำหรับการแสดงผลใน Grid ทั้ง 3 ช่อง
        internal static List<INVENTTRANSFER> GetAndroidOrderToMain(string _DateDoc, string _Dept, string _StaApvDoc, string _StaAx, string _groupItem, string _round, MainPrint.StatusGridView _Orderby)
        {
            //string sql, condDept, condItem = "", condRound, condOrderby = "";
            //List<INVENTTRANSFER> List = new List<INVENTTRANSFER>();

            ////if (_Dept.Equals("D147") || _Dept.Equals("D019") || _Dept.Equals("D067"))
            ////{
            ////    if (_Dept.Equals("D147"))
            ////        condDept = $"AND DeptGroup IN ({GetDeptUse(_Dept)}) AND DEPT IN ('{_Dept}')";
            ////    else
            ////        condDept = $"AND DeptGroup = '{_Dept}'  AND DEPT = '{_Dept}'";
            ////}
            ////else
            ////{
            //    condDept = $"AND Depart = '{_Dept}' AND ANDROID_ROUTE.DEPT = '{_Dept}' AND ANDROID_ROUTE.STATUS = '1'";
            ////}

            //if (!_groupItem.Equals("0")) condItem = $"AND ANDROID_ORDERTOHD.GroupItem ='{_groupItem}'";


            //if (_round.Equals("3")) condRound = "AND ANDROID_ORDERTOHD.[Round] in ('1','2')";
            //else condRound = $"AND ANDROID_ORDERTOHD.[Round] = '{_round}'";

            //if (_Orderby == MainPrint.StatusGridView.Document) condOrderby = "Order by DocNo";
            //else if (_Orderby == MainPrint.StatusGridView.Approve) condOrderby = "Order by DateApv,TimeApv";
            //else if (_Orderby == MainPrint.StatusGridView.Ax) condOrderby = "Order by DateAx,TimeAx";

            //sql = $@"
            //    SELECT  DOCNO,convert(varchar,Date,23) +' '+ Time AS DATEDOCNO ,
            //            SHOP_BRANCH.BRANCH_ID+' : '+SHOP_BRANCH.BRANCH_NAME AS BRANCH_NAME,
            //            SPC_ROUTETABLE.ROUTEID AS ROUTEID ,
            //            SPC_ROUTETABLE.NAME   AS ROUTENAME,
            //            CASE StaAx WHEN '1' THEN 'ส่งบิลเรียบร้อย' ELSE    
            //            CASE StaDoc WHEN '1' THEN 'กำลังจัดสินค้า' ELSE 
            //            CASE StaPrcDoc WHEN '1' THEN 'เอกสารเรียบร้อย' ELSE 
            //            CASE StaApvDoc  WHEN '1' THEN 'จัดสินค้าเรียบร้อย'ELSE 
            //            'เอกสารยกเลิก' END END END END AS STATUSDOCNO,
            //            CASE depart WHEN 'D110' THEN 'ผัก'  
            //            WHEN 'D164' THEN 'ผลไม้'  
            //            WHEN 'D144' THEN 'ปลา' 
            //            WHEN 'D067' THEN 'วราวรรณ' 
            //            WHEN 'D032' THEN 'หมู-ไก่' 
            //            WHEN 'D106' THEN 'ปาณิสรา' 
            //            WHEN 'D167' THEN 'วราวรรณ' 
            //            WHEN 'D027' THEN 'สาลีนี'  ELSE '' END AS 'ITEMTYPE', 
            //            isnull(Box,'1') AS BOX,isnull(BoxPrint,'0') AS BOXPRINT,ISNULL(NULLIF(ANDROID_ORDERTOHD.WhoApv,''),ANDROID_ORDERTOHD.WhoIn) AS WHOAPV,ANDROID_ORDERTOHD.WhoUp AS WHOUP,'Print' AS PRINTED 
            //    FROM    ANDROID_ORDERTOHD  WITH (NOLOCK) INNER JOIN 
            //            ANDROID_ROUTE  WITH (NOLOCK) ON ANDROID_ORDERTOHD.Branch = ANDROID_ROUTE.ACCOUNTNUM INNER JOIN 
            //            SHOP_BRANCH  WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID =ANDROID_ROUTE.ACCOUNTNUM INNER JOIN   
            //            SHOP2013TMP.dbo.SPC_ROUTETABLE  WITH (NOLOCK) ON ANDROID_ROUTE.ROUTEID =  SPC_ROUTETABLE.ROUTEID  
            //            AND SHOP_BRANCH.BRANCH_ID = ANDROID_ROUTE.ACCOUNTNUM    
            //    WHERE   StaDoc = '1'  AND  StaPrcDoc != '3'   
            //            AND StaApvDoc= {_StaApvDoc}  AND StaAx = {_StaAx}
            //            AND date='{_DateDoc}'  
            //            {condDept}
            //            AND  SPC_ROUTETABLE.ROUTEID IN (SELECT DISTINCT ROUTEID  FROM ANDROID_ROUTE WHERE STATUS='1' AND DEPT='{_Dept}')
            //            AND SPC_ROUTETABLE.DATAAREAID='SPC'
            //            {condItem} {condRound} {condOrderby} ";

            /// **** ด้านบน code เดิม ยังไม่ลบ
            string sql, condDept, condItem = "", condRound, condOrderby = "";
            List<INVENTTRANSFER> List = new List<INVENTTRANSFER>();

            condDept = $"AND Depart = '{_Dept}' ";

            if (!_groupItem.Equals("0")) condItem = $"AND ANDROID_ORDERTOHD.GroupItem ='{_groupItem}'";

            if (_round.Equals("3")) condRound = "AND ANDROID_ORDERTOHD.[Round] in ('1','2')";
            else condRound = $"AND ANDROID_ORDERTOHD.[Round] = '{_round}'";

            if (_Orderby == MainPrint.StatusGridView.Document) condOrderby = "Order by DocNo";
            else if (_Orderby == MainPrint.StatusGridView.Approve) condOrderby = "Order by DateApv,TimeApv";
            else if (_Orderby == MainPrint.StatusGridView.Ax) condOrderby = "Order by DateAx,TimeAx";

            sql = $@"
                SELECT  DOCNO,convert(varchar,Date,23) +' '+ Time AS DATEDOCNO ,
                        SHOP_BRANCH.BRANCH_ID+' : '+SHOP_BRANCH.BRANCH_NAME AS BRANCH_NAME,
                        ROUTEID AS ROUTEID ,
                        NAME   AS ROUTENAME,
                        CASE StaAx WHEN '1' THEN 'ส่งบิลเรียบร้อย' ELSE    
                        CASE StaDoc WHEN '1' THEN 'กำลังจัดสินค้า' ELSE 
                        CASE StaPrcDoc WHEN '1' THEN 'เอกสารเรียบร้อย' ELSE 
                        CASE StaApvDoc  WHEN '1' THEN 'จัดสินค้าเรียบร้อย'ELSE 
                        'เอกสารยกเลิก' END END END END AS STATUSDOCNO,StaAx AS STATUS,
                        DIMENSIONS.DESCRIPTION AS ITEMTYPE,
                        isnull(Box,'1') AS BOX,isnull(BoxPrint,'0') AS BOXPRINT,ISNULL(NULLIF(ANDROID_ORDERTOHD.WhoApv,''),
                        ANDROID_ORDERTOHD.WhoIn) AS WHOAPV,ANDROID_ORDERTOHD.WhoUp AS WHOUP,'Print' AS PRINTED 
                FROM    ANDROID_ORDERTOHD  WITH (NOLOCK) 
		                INNER JOIN (
			                SELECT	ACCOUNTNUM,SPC_ROUTETABLE.ROUTEID,SPC_ROUTETABLE.NAME	
			                FROM	(
					                SELECT	ACCOUNTNUM,ROUTEID,ROW_NUMBER( ) OVER(PARTITION BY ACCOUNTNUM ORDER BY ROUTEID ) AS SEQ
					                FROM	[SHOP2013TMP].[dbo].[SPC_ROUTINGPOLICY]
					                WHERE	ACCOUNTNUM LIKE 'MN%'
							                AND ISPRIMARY = '1'		
					                )TMP INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON TMP.ROUTEID = SPC_ROUTETABLE.ROUTEID
			                WHERE	SEQ = 1 AND SPC_ROUTETABLE.DATAAREAID = 'SPC')ANDROID_ROUTE ON ANDROID_ORDERTOHD.Branch = ANDROID_ROUTE.ACCOUNTNUM 
		                INNER JOIN SHOP_BRANCH  WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID =ANDROID_ROUTE.ACCOUNTNUM 
                        INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON ANDROID_ORDERTOHD.Depart = DIMENSIONS.NUM AND DIMENSIONCODE = '0'
							AND DIMENSIONS.DATAAREAID = N'SPC'
                WHERE   StaDoc = '1'  AND  StaPrcDoc != '3'   
                        AND StaApvDoc= {_StaApvDoc}  AND StaAx = {_StaAx}
                        AND date='{_DateDoc}'  
                        {condDept}
                        {condItem} {condRound} 
                {condOrderby} ";

            DataTable dt = ConnectionsDB.GetSelectSQL(sql);
            foreach (DataRow row in dt.Rows)
            {
                INVENTTRANSFER Tmp = new INVENTTRANSFER()
                {
                    DOCNO = row["DOCNO"].ToString(),
                    DATEDOC = row["DATEDOCNO"].ToString(),
                    BRANCHNAME = row["BRANCH_NAME"].ToString(),
                    ROUTEID = row["ROUTEID"].ToString(),
                    ROUTENAME = row["ROUTENAME"].ToString(),
                    STATUS = row["STATUS"].ToString(),
                    ITEMTYPE = row["ITEMTYPE"].ToString(),
                    COUNTBOX = Convert.ToInt32(row["BOX"].ToString()),
                    WHOAPV = row["WHOAPV"].ToString(),
                    WHOUP = row["WHOUP"].ToString(),
                    PRINTTAG = row["PRINTED"].ToString(),
                    BOXPRINT = Convert.ToInt32(row["BOXPRINT"].ToString())
                };
                List.Add(Tmp);
            }
            return List;
        }

        //internal static List<COUNTBOX> GetCountBoxListFromAndroidPosBoxSheet(string _status, string _pConfig)
        //{
        //    if (_status.Equals("0"))
        //    {
        //        _status = "";
        //    }
        //    else
        //    {
        //        _status = @"AND STATUSPRINT = '0'";
        //    }
        //    string sql = string.Format(@"
        //            SELECT VOUCHERID,count(isnull(REFBOXTRANS,0)) AS CBOX   
        //            FROM    {1}ANDROID_POSTOBOXFACESHEET WITH (NOLOCK)
        //            WHERE   convert(varchar,shipdate,23) between getdate()-2 and getdate() {0}
        //            GROUP BY VOUCHERID
        //            ", _status
        //            , _pConfig);

        //    DataTable dt = ConnectionsDB.GetSelectSQL(sql);
        //    List<COUNTBOX> List = new List<COUNTBOX>();
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        COUNTBOX Tmp = new COUNTBOX()
        //        {
        //            BOX = Convert.ToInt32(row["CBOX"]),
        //            VOUCHERID = row["VOUCHERID"].ToString()
        //        };
        //        List.Add(Tmp);
        //    }
        //    return List;
        //}
        #endregion

        #region BoxFaceSheet
        //OK ค้นหาจำนวนลังที่พิมแล้วหรือยังไม่พิม  ANDROID_POSTOBOXFACESHEET
        internal static int GetCountBoxFaceSheet(string _Mrt, string _statusAll)
        {
            if (_statusAll.Equals("0")) _statusAll = "";
            else _statusAll = "AND STATUSPRINT='0'";

            string sql = $@"
                    SELECT COUNT(isnull(REFBOXTRANS,0)) AS CBox 
                    FROM   ANDROID_POSTOBOXFACESHEET WITH (NOLOCK) 
                    WHERE  VOUCHERID = '{_Mrt}' AND DATAAREAID = N'SPC' {_statusAll}
                    ";
            return Convert.ToInt32(ConnectionsDB.GetSelectSQL(sql).Rows[0]["CBox"].ToString());
        }
        //OK สำหรับการส่งค่าเข้า AX
        internal static List<BOXTRANS> GetBoxTrans(string _Mrt)
        {
            string sql = $@"
                    SELECT  ROW_NUMBER() OVER(ORDER BY Refboxtrans ASC) AS ROW,REFPALLETTRANS,REFBOXTRANS,ITEMBARCODE,LINECAPACITY
		                    ,LINEWEIGHT,TRANSDATE,BOXQTY,BOXTYPE,[NAME]
		                    ,BOXGROUP,VOUCHERID,DATAAREAID,RECVERSION,RECID,SEQ,[STATUS]
                    FROM    ANDROID_POSTOBOXTRANS WITH (NOLOCK) 
                    WHERE   VOUCHERID = '{_Mrt}' AND [STATUS] = '0'
                    ";
            DataTable dt = ConnectionsDB.GetSelectSQL(sql);
            List<BOXTRANS> List = new List<BOXTRANS>();
            foreach (DataRow row in dt.Rows)
            {
                BOXTRANS Tmp = new BOXTRANS()
                {
                    ROW = Convert.ToInt32(row["ROW"]),
                    REFPALLETTRANS = row["REFPALLETTRANS"].ToString(),
                    REFBOXTRANS = row["REFBOXTRANS"].ToString(),
                    ITEMBARCODE = row["ITEMBARCODE"].ToString(),
                    LINECAPACITY = row["LINECAPACITY"].ToString(),
                    LINEWEIGHT = row["LINEWEIGHT"].ToString(),
                    BOXQTY = row["BOXQTY"].ToString(),
                    BOXTYPE = row["BOXTYPE"].ToString(),
                    NAME = row["NAME"].ToString(),
                    RECVERSION = row["RECVERSION"].ToString(),
                    RECID = row["RECID"].ToString()
                };
                List.Add(Tmp);
            }
            return List;
        }
        //OK สำหรับการส่งค่าเข้า AX
        internal static List<BOXFACESHEET> GetBoxFacesheet(string _Mrt)
        {
            string sql = $@"
                    SELECT  ROW_NUMBER() OVER(ORDER BY Refboxtrans ASC) AS Row,SHIPPINGDATEREQUESTED,REFBOXTRANS,BOXGROUP,QTY
                            ,NUMBERLABEL,ROUTENAME,DLVNAME,BOXNAME,BOXTYPE
                            ,LOCATIONNAMETO,INVENTLOCATIONIDTO,SHIPDATE,VOUCHERID,DATAAREAID
                            ,RECVERSION,RECID
                    FROM    ANDROID_POSTOBOXFACESHEET WITH (NOLOCK) 
                    WHERE   VOUCHERID = '{_Mrt}' AND [STATUS] = '0' AND DATAAREAID = N'SPC'
                    ";
            DataTable dt = ConnectionsDB.GetSelectSQL(sql);
            List<BOXFACESHEET> List = new List<BOXFACESHEET>();
            foreach (DataRow row in dt.Rows)
            {
                BOXFACESHEET Tmp = new BOXFACESHEET()
                {
                    ROW = Convert.ToInt32(row["ROW"]),
                    SHIPPINGDATEREQUESTED = row["SHIPPINGDATEREQUESTED"].ToString(),
                    REFBOXTRANS = row["REFBOXTRANS"].ToString(),
                    QTY = row["QTY"].ToString(),
                    ROUTENAME = row["ROUTENAME"].ToString(),
                    DLVNAME = row["DLVNAME"].ToString(),
                    BOXNAME = row["BOXNAME"].ToString(),
                    BOXTYPE = row["BOXTYPE"].ToString(),
                    LOCATIONNAMETO = row["LOCATIONNAMETO"].ToString(),
                    INVENTLOCATIONIDTO = row["INVENTLOCATIONIDTO"].ToString(),
                    RECVERSION = row["RECVERSION"].ToString(),
                    RECID = row["RECID"].ToString(),
                    BOXGROUP = row["BOXGROUP"].ToString()
                };
                List.Add(Tmp);
            }
            return List;
        }
        //OK ค้นหาเลขที่ลังทั้งหมด ANDROID_POSTOBOXFACESHEET
        internal static DataTable GetBoxFacesheetByVoucherid(string voucherId)
        {
            string sql = $@"
                    SELECT  CONVERT(VARCHAR,SHIPDATE,102) AS SHIPDATE,BOXNAME,BOXGROUP,VOUCHERID,NUMBERLABEL
		                    ,INVENTLOCATIONIDTO+','+LOCATIONNAMETO AS LOCATIONNAMETO,ROUTENAME,CONVERT(VARCHAR,GETDATE(),25) AS SETDATETIME  
                    FROM	ANDROID_POSTOBOXFACESHEET WITH (NOLOCK)  
                    WHERE   VOUCHERID='{voucherId}' 
                            AND STATUSPRINT='0' AND DATAAREAID = N'SPC' 
                    ORDER BY BOXGROUP DESC
                    ";
            return ConnectionsDB.GetSelectSQL(sql);
        }
        //OK สำหรับการพิมพ์บิลซ้ำ
        internal static DataTable GetBoxFaceSheet(string _Doc)//สำหรับการส่งค่าเข้า AX
        {
            string sql = $@"
                    SELECT  '0' AS CHOOSE,CONVERT(VARCHAR, SHIPDATE, 102) AS SHIPDATE, 'ลังสูง' AS BOXNAME, BOXGROUP, VOUCHERID AS VOUCHERID, NUMBERLABEL,
		                    INVENTLOCATIONIDTO +',' + LOCATIONNAMETO AS LOCATIONNAMETO, ROUTENAME, CONVERT(varchar, GETDATE(), 25) AS SETDATETIME
                    FROM    ANDROID_POSTOBOXFACESHEET WITH(NOLOCK)
                    WHERE   VOUCHERID = '{_Doc}'
                    ORDER BY   REFBOXTRANS 
            ";

            //string sql = $@"
            //        SELECT  '0' AS CHOOSE,CONVERT(VARCHAR, TRANSDATE, 102) AS SHIPDATE, NAME AS BOXNAME, BOXGROUP, DOCUMENTNUM AS VOUCHERID, NUMBERLABEL,
            //                INVOICEACCOUNT +',' + CUSTNAME AS LOCATIONNAMETO, ROUTENAME, CONVERT(varchar, GETDATE(), 25) AS SETDATETIME
            //        FROM    SHOP2013TMP.dbo.SPC_BOXFACESHEET WITH(NOLOCK)
            //        WHERE   DOCUMENTNUM = '{_Doc}'
            //        ORDER BY   REFBOXTRANS ";

            return Controllers.ConnectionsDB.GetSelectSQL(sql);
        }

        //OK ค้นหาเลขที่ลัง ANDROID_POSTOBOXFACESHEET
        internal static string[] GetBoxSheet(string _Doc)//, string _typeBill
        {
            //string sql;
            //if (_typeBill.Equals("T"))
            //{
            //    sql = $@"
            //        SELECT BOXGROUP
            //        FROM   SHOP2013TMP.dbo.SPC_BOXFACESHEET  WITH (NOLOCK)   
            //        WHERE   DOCUMENTNUM = '{_Doc}' AND DATAAREAID = N'SPC' 
            //        ";
            //}
            //else
            //{
            string sql = $@"
                    SELECT  BOXGROUP 
                    FROM    ANDROID_POSTOBOXFACESHEET  WITH (NOLOCK)  
                    WHERE   VOUCHERID = '{_Doc}' 
                            AND DATAAREAID='SPC'  AND STATUS='0' 
                    ";
            //}
            DataTable dt = Controllers.ConnectionsDB.GetSelectSQL(sql);
            string[] itemgroup = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                itemgroup[i] = dt.Rows[i]["BOXGROUP"].ToString();
            }
            return itemgroup;
        }
        #endregion

        #region AndroidPosTo
        //OK ค้นหาข้อมูล DT
        internal static DataTable GetAndroidPosLine(string _Doc)
        {
            string sql = $@"
                    SELECT  LINENUM,QTY,ITEMID,[NAME],SALESUNIT,ITEMBARCODE,INVENTDIMID,SALESPRICE 
                    FROM	ANDROID_POSTOTABLE  WITH(NOLOCK) 
                            INNER JOIN  ANDROID_POSTOLINE  WITH(NOLOCK)  ON ANDROID_POSTOTABLE.VOUCHERID = ANDROID_POSTOLINE.VOUCHERID 
                    WHERE	ANDROID_POSTOTABLE.TRANSFERID = '{_Doc}' 
                    ORDER BY LINENUM
                    ";

            return Controllers.ConnectionsDB.GetSelectSQL(sql);
        }
        //OK ค้นหาข้อมูล HD
        internal static ANDROIDPOSTOTABLE GetAndroidPosTable(string _Doc)
        {
            string sql = $@"
                SELECT  BRANCH_NAME+','+INVENTLOCATIONIDTO AS INVENTLOCATIONIDTO,INVENTLOCATIONIDFROM,VOUCHERID,TRANSFERID,
                        convert(varchar, SHIPDATE, 23) AS SHIPDATE, SPC_NAME + ',' + CASHIERID AS CASHIERID, ROUTESHIPMENTID, convert(varchar, GETDATE(), 25) AS SETDATETIME
                FROM    ANDROID_POSTOTABLE WITH(NOLOCK) 
                        INNER JOIN SHOP2013TMP.dbo.[EMPLTABLE] WITH (NOLOCK)  ON ANDROID_POSTOTABLE.CASHIERID = EMPLTABLE.EMPLID 
                        INNER JOIN SHOP_BRANCH WITH (NOLOCK)  ON ANDROID_POSTOTABLE.INVENTLOCATIONIDTO = SHOP_BRANCH.BRANCH_ID
                WHERE  TRANSFERID = '{_Doc}'
                        AND ANDROID_POSTOTABLE.DATAAREAID = N'SPC' 
                        AND EMPLTABLE.DATAAREAID = N'SPC'
                ";

            DataTable dt = Controllers.ConnectionsDB.GetSelectSQL(sql);
            ANDROIDPOSTOTABLE Tmp = new ANDROIDPOSTOTABLE();
            foreach (DataRow row in dt.Rows)
            {
                Tmp.INVENTLOCATIONIDTO = row["INVENTLOCATIONIDTO"].ToString();
                Tmp.INVENTLOCATIONIDFROM = row["INVENTLOCATIONIDFROM"].ToString();
                Tmp.VOUCHERID = row["VOUCHERID"].ToString();
                Tmp.TRANSFERID = row["TRANSFERID"].ToString();
                Tmp.SHIPDATE = row["SHIPDATE"].ToString();
                Tmp.CASHIERID = row["CASHIERID"].ToString();
                Tmp.ROUTESHIPMENTID = row["ROUTESHIPMENTID"].ToString();
                Tmp.SETDATETIME = row["SETDATETIME"].ToString();
            }
            return Tmp;
        }
        #endregion

        #region ItemActive
        //OK ค้นหาข้อมูล DT ใน ANDROID_ORDERTODT เพื่อนำไปส่งข้อมูลต่อ
        internal static List<ANDROIDORDER> GetAndroidOrderCheckItemActive(string _Doc, string _round)
        {
            //string round;
            //if (!_round.Equals("3"))
            //{
            //    round = $"AND ANDROID_ORDERTOHD.[Round] = '{_round}' ";//AND ANDROID_ORDERTODT.[Round] = '{_round}'
            //}
            //else
            //{
            //    round = $"AND ANDROID_ORDERTODT.[Round] = ( SELECT [Round] FROM ANDROID_ORDERTOHD WHERE Docno='{_Doc}')";
            //}

            string round = $"AND ANDROID_ORDERTOHD.[Round] = '{_round}' ";
            if (_round == "3") round = $"AND ANDROID_ORDERTOHD.[Round] IN ('1','2') ";

            string sql = $@"
                    SELECT  SeqNo,ItemDim,ANDROID_ORDERTODT.ItemID,Barcode,Name,ANDROID_ORDERTODT.Qty, 
		                    ANDROID_ORDERTODT.UnitID,Price,ANDROID_ORDERTOHD.WhoApv,Date, 
		                    Branch,SPC_SALESPRICETYPE 
                    FROM    ANDROID_ORDERTOHD WITH (NOLOCK) 
                            INNER JOIN ANDROID_ORDERTODT WITH (NOLOCK)   ON ANDROID_ORDERTOHD.Docno= ANDROID_ORDERTODT.DocNo  
                            INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON ANDROID_ORDERTODT.Barcode= INVENTITEMBARCODE.ITEMBARCODE    
                    WHERE   ANDROID_ORDERTOHD.Docno = '{_Doc}'   
                            AND ANDROID_ORDERTODT.Qty > 0   
	                        {round}
	                        AND INVENTITEMBARCODE.DATAAREAID = N'SPC' AND INVENTITEMBARCODE.SPC_ITEMACTIVE = '1' 
                    ORDER BY SeqNo
                    ";
            DataTable dt = ConnectionsDB.GetSelectSQL(sql);

            List<ANDROIDORDER> List = new List<ANDROIDORDER>();
            foreach (DataRow row in dt.Rows)
            {
                ANDROIDORDER Tmp = new ANDROIDORDER()
                {
                    SEQNO = Convert.ToInt32(row["SeqNo"]),
                    ITEMDIM = row["ItemDim"].ToString(),
                    ITEMID = row["ItemID"].ToString(),
                    ITEMBARCODE = row["Barcode"].ToString(),
                    NAME = row["Name"].ToString(),
                    QTY = Convert.ToDouble(row["Qty"]),
                    UNITID = row["UnitID"].ToString(),
                    PRICE = Convert.ToDouble(row["Price"]),
                    WHOAPV = row["WhoApv"].ToString(),
                    DATE = row["Date"].ToString(),
                    BRANCH = row["Branch"].ToString(),
                    SPC_SALESPRICETYPE = int.Parse(row["SPC_SALESPRICETYPE"].ToString())
                };
                List.Add(Tmp);
            }
            return List;
        }
        #endregion

        #region UseDept
        //static string GetDeptUse(string _detp, string strdept = "")
        //{
        //    string sql = string.Format(@"
        //            SELECT  [DEPTID]
        //                    ,[STATUS]
        //                    ,[DEPT_ROUTE]
        //                    ,[DEPT_USE]
        //            FROM    [ANDROID_DEPT] WITH (NOLOCK)
        //            WHERE   DEPT_USE = '{0}'
        //            ", _detp);
        //    DataTable dt = Controllers.ConnectionsDB.GetSelectSQL(sql);
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        strdept += $"'{row["DEPTID"].ToString()}',";
        //    }
        //    return strdept.Substring(0, strdept.Length - 1);
        //}

        //OK
        internal static DataTable GetDeptUseAndroidDept(string _dept)
        {
            string sql = $@" 
                SELECT  [DEPTID],[STATUS],[DEPT_ROUTE],[DEPT_USE],[DESCRIPTION]
                FROM    [ANDROID_DEPT] WITH(NOLOCK) 
		                INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH(NOLOCK) ON ANDROID_DEPT.DEPTID = DIMENSIONS.NUM 
                WHERE   [DEPT_USE] = '{_dept}' AND STATUS = '1' ";
            return Controllers.ConnectionsDB.GetSelectSQL(sql);
        }
        #endregion

        #region UpdateStatusPrint
        //OK Update การพิมพ์ tag
        internal static string UpdateStatusPrint(string _VoucherID)
        {
            string SqlUpdate = $@"
                    UPDATE  ANDROID_POSTOBOXFACESHEET 
                    SET     STATUSPRINT = '1'  
                    WHERE   VOUCHERID = '{_VoucherID}' AND STATUSPRINT = '0'
                    ";
            string ret = ConnectionsDB.ExecuteSQL_Main(SqlUpdate);
            return string.IsNullOrEmpty(ret) ? ret : ret + '\n' + "[UPDATE STATUSPRINT=1 TBL ANDROID_POSTOBOXFACESHEET]";
        }
        #endregion

        //พิมพ์เอกสาร Tag
        internal static void PrintBoxFaceSheet30x40_GoDEX(string _printerName, DataTable dt_DetailTag)
        {
            if (dt_DetailTag == null || dt_DetailTag.Rows.Count == 0) return;

            StringBuilder rowData = new StringBuilder();
            int iBox = 1;
            foreach (DataRow row in dt_DetailTag.Rows)
            {
                string Dept = string.Empty;
                Dept = row["VOUCHERID"].ToString().Substring(9, 3);
                string NumLabel = row["NUMBERLABEL"].ToString();
                if (NumLabel == "") NumLabel = "[" + iBox.ToString() + "/" + dt_DetailTag.Rows.Count.ToString() + "]"; else NumLabel = "[" + NumLabel + "]";

                rowData.Append("^Q38,3\n");
                rowData.Append("^W28\n");
                rowData.Append("^H0\n");
                rowData.Append("^P1\n");
                rowData.Append("^S6\n");
                rowData.Append("^AD\n");
                rowData.Append("^C1\n");
                rowData.Append("^R0\n");
                rowData.Append("~Q+0\n");
                rowData.Append("^O0\n");
                rowData.Append("^D0\n");
                rowData.Append("^E16\n");
                rowData.Append("~R200\n");
                rowData.Append("^XSET,ROTATION,0\n");
                rowData.Append("^L\n");
                rowData.Append("Dy2-me-dd\n");
                rowData.Append("Th:m:s\n");
                rowData.Append("Dy2-me-dd\n");
                rowData.Append("Th:m:s\n");

                // typeItem, row("NUMBERLABEL")
                rowData.AppendFormat("ATA,226,0,28,28,0,1,A,0,{0}\n", row["BOXNAME"]);
                rowData.AppendFormat("BQ,185,16,2,6,50,1,0,{0}\n", row["BOXGROUP"]);
                rowData.AppendFormat("ATA,135,2,30,30,0,1,A,0,{0}    {1}\n", row["BOXGROUP"], DateTime.Parse(row["SHIPDATE"].ToString()).ToString("dd.MM.yy"));
                rowData.AppendFormat("ATA,110,0,30,30,0,1,A,0,{0} \n", row["VOUCHERID"]);//, NumLabel
                rowData.AppendFormat("ATA,85,0,30,30,0,1,A,0,{0}\n", row["ROUTENAME"]);
                rowData.AppendFormat("ATA,56,0,42,42,0,1,A,0,{0}\n", row["LOCATIONNAMETO"]);
                rowData.AppendFormat("ATA,108,240,28,28,0,1,A,0,{0}\n", NumLabel);
                rowData.Append("E\n");
                iBox++;
            }
            RawPrinterHelper.SendStringToPrinter(_printerName, rowData.ToString());
        }


        ////OK
        //internal static List<POSTOBOXFACESHEET> GetBoxFaceSheet(string _Doc, string _T = "")
        //{
        //    string sql;
        //    if (_T.Equals(""))
        //    {
        //        sql = $@"
        //            SELECT  CONVERT(VARCHAR, SHIPDATE,102) AS SHIPDATE, BOXNAME, BOXGROUP, VOUCHERID AS DOCUMENTNUM, NUMBERLABEL,
        //                    INVENTLOCATIONIDTO+',' + LOCATIONNAMETO AS LOCATIONNAMETO,
        //                    ROUTENAME, CONVERT(varchar, GETDATE(), 25) AS SETDATETIME
        //            FROM    ANDROID_POSTOBOXFACESHEET   WITH(NOLOCK)
        //            WHERE   VOUCHERID = '{_Doc}'
        //                    AND STATUS = '0' AND DATAAREAID = N'SPC' 
        //            ORDER BY SEQ DESC
        //            ";
        //    }
        //    else
        //    {
        //        sql = $@"
        //            SELECT  CONVERT(VARCHAR,TRANSDATE,102) AS SHIPDATE,BOXTYPE , BOXGROUP,DOCUMENTNUM,NUMBERLABEL, 
        //                    INVOICEACCOUNT +','+CUSTNAME AS LOCATIONNAMETO, 
        //                    ROUTENAME,CONVERT(varchar,GETDATE(),25) AS SETDATETIME
        //            FROM    SHOP2013TMP.dbo.SPC_BOXFACESHEET WITH(NOLOCK)  
        //            WHERE   DATAREARID = N'SPC' AND DOCUMENTNUM = '{_Doc}'  
        //            ORDER BY   NUMBERLABEL
        //            ";
        //    }
        //    DataTable dt = Controllers.ConnectionsDB.GetSelectSQL(sql);
        //    List<POSTOBOXFACESHEET> List = new List<POSTOBOXFACESHEET>();
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        POSTOBOXFACESHEET Tmp = new POSTOBOXFACESHEET()
        //        {
        //            SHIPDATE = row["SHIPDATE"].ToString(),
        //            BOXGROUP = row["BOXGROUP"].ToString(),
        //            DOCUMENTNUM = row["DOCUMENTNUM"].ToString(),
        //            NUMBERLABEL = row["NUMBERLABEL"].ToString(),
        //            LOCATIONNAMETO = row["LOCATIONNAMETO"].ToString(),
        //            ROUTENAME = row["ROUTENAME"].ToString(),
        //            SETDATETIME = row["SETDATETIME"].ToString()
        //        };
        //        List.Add(Tmp);
        //    }
        //    return List;
        //}

    }
}
