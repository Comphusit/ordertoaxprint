﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using OrderToAx.Order.Controllers;
using Microsoft.Reporting.WinForms;
using OrderToAx.Order.Table;
using System.Collections;

namespace OrderToAx.Order
{
    public partial class MainPrint : Telerik.WinControls.UI.RadForm
    {
        #region Fields 
        public enum StatusGridView
        {
            Document = 1,
            Approve = 2,
            Ax = 3
        }

        private const int count = 30;
        private const int lastDate = 40;
        private const int Frow = 0;

        private DateTime recentTime;

        string documentDate;
        string Round;
        string GroupItem;

        private readonly DataTable dt_Document = new DataTable();
        private readonly DataTable dt_Approve = new DataTable();
        private readonly DataTable dt_Ax = new DataTable();

        //string Tmp;
        #endregion
        public MainPrint()
        {
            InitializeComponent();
            this.InitializeForm();
            this.InitializeTimer();
        }

        #region InitializeForm
        void InitializeForm()
        {
            this.Load += new EventHandler(MainPrint_Load);
        }
        void InitializeTimer()
        {
            this.Timer1.Tick += new EventHandler(this.Timer_Tick);
        }
        //mainLoad
        private void MainPrint_Load(object sender, EventArgs e)
        {
            Connections.SystemComName = Environment.MachineName;
            Connections.SystemDept = PrintQueryClass.GetDepByComName(Connections.SystemComName);

            if (Connections.SystemDept.Equals(""))//|| Connections.SystemDeptCurent.Equals("")
            {
                MessageBox.Show($@"เครื่องนี้ไม่มีสิทธ์ใช้งานโปรแกรม {Connections.AppName}{Environment.NewLine}โทร 8570 หรือส่งเมลล์ที่ ComMinimart.", Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
                return;
            }

            PrintQueryClass.UpdateUse();
            this.InitializeGrid();

            using (SelectPrinter selectPrinter = new SelectPrinter())
            {
                DialogResult dr = selectPrinter.ShowDialog();
                if (dr == DialogResult.Yes)
                {
                    this.InitializeLoad();
                }
                else
                {
                    Application.Exit();
                    return;
                }
            }
        }
        void InitializeLoad()
        {
            this.reportViewer1.Dock = DockStyle.Fill;
            this.Text = Connections.AppName;

            Round = "1";
            GroupItem = "0";
            radDropDownList_GroupItem.Enabled = false;
            recentTime = DateTime.Now;//For Counter

            Property.SetFontDropDown(radDropDownList_GroupItem, Connections.FontTahoma10, Color.Black, radDropDownList_GroupItem.DropDownListElement.DropDownWidth, radDropDownList_GroupItem.DropDownListElement.DropDownHeight);
            Property.SetDefaultFontDateTimePicker(radDateTimePicker_Date, DateTime.Now, Connections.FontTahoma12);
            documentDate = radDateTimePicker_Date.Value.ToString("yyyy-MM-dd");

            Timer1.Start();
            this.SetButtonDept();
            this.BindDropDownListGroupItem(PrintQueryClass.GetGroupItem(Connections.SystemDept));
            this.GetDataBillToGrid(StatusGridView.Ax);
        }
        #endregion

        #region Timer
        private void Timer_Tick(object sender, EventArgs e)
        {
            DateTime _recentTime = recentTime.AddSeconds(count);
            if (_recentTime < DateTime.Now)
            {
                if (radDateTimePicker_Date.Value > DateTime.Now.AddDays(-lastDate))
                {
                    this.Cursor = Cursors.WaitCursor;

                    if (RadGridView_Apv.Rows.Count > 0)
                    {
                        try
                        {
                            Timer1.Stop();
                            string document = InsertPosTable(Frow);
                            if (!document.Equals("0") && !document.Equals(""))
                            {
                                PrintDocument(PrintQueryClass.GetAndroidPosTable(document), PrintQueryClass.GetAndroidPosLine(document),
                                    PrintQueryClass.GetBoxSheet(GetVoucherID(document)));

                                string docnoAx = document;
                                string MRT = GetVoucherID(docnoAx);

                                if (PrintQueryClass.GetCountBoxFaceSheet(MRT, "1") > 0)
                                {
                                    DataTable dt_BoxFacesheet = PrintQueryClass.GetBoxFacesheetByVoucherid(MRT);
                                    PrintQueryClass.PrintBoxFaceSheet30x40_GoDEX(Connections.PrintTag, dt_BoxFacesheet);
                                    PrintQueryClass.UpdateStatusPrint(MRT);
                                }
                                else
                                {
                                    DataTable dt_BoxFacesheet = PrintQueryClass.GetBoxFacesheetByVoucherid(MRT);
                                    PrintQueryClass.PrintBoxFaceSheet30x40_GoDEX(Connections.PrintTag, dt_BoxFacesheet);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            recentTime = DateTime.Now;
                            Timer1.Stop();
                        }
                        finally
                        {
                            Timer1.Start();
                            recentTime = DateTime.Now;
                        }
                    }

                    this.GetDataBillToGrid(StatusGridView.Ax);

                    this.Cursor = Cursors.Default;
                    recentTime = DateTime.Now;
                }
                else
                {
                    recentTime = DateTime.Now;
                }
            }
        }

        #endregion

        #region DateTimePicker
        private void RadDateTimePicker_Date_ValueChanged(object sender, EventArgs e)
        {
            documentDate = radDateTimePicker_Date.Value.ToString("yyyy-MM-dd");
            this.GetDataBillToGrid(StatusGridView.Ax);
        }
        #endregion

        #region InitializeDropDown
        private void BindDropDownListGroupItem(DataTable _dt)
        {
            radDropDownList_GroupItem.DataSource = _dt;
            radDropDownList_GroupItem.ValueMember = "ITEMGROUPID";
            radDropDownList_GroupItem.DisplayMember = "ITEMGROUPNAME";
        }
        private void RadCheckBox_GroupItem_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_GroupItem.CheckState == CheckState.Checked)
            {
                radDropDownList_GroupItem.Enabled = true;
                GroupItem = radDropDownList_GroupItem.SelectedValue == null ? "0" : radDropDownList_GroupItem.SelectedValue.ToString();
            }
            else
            {
                radDropDownList_GroupItem.Enabled = false;
                GroupItem = "0";
            }
            this.GetDataBillToGrid(StatusGridView.Ax);
        }
        private void RadDropDownList_GroupItem_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (radCheckBox_GroupItem.Checked == true)
            {
                GroupItem = radDropDownList_GroupItem.SelectedValue == null ? "0" : radDropDownList_GroupItem.SelectedValue.ToString();
                this.GetDataBillToGrid(StatusGridView.Ax);
            }
        }

        #endregion

        #region InitializeRadioButton
        private void RadRadioButton_Rout1_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadioButton_Round1.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                Round = "1";
                this.GetDataBillToGrid(StatusGridView.Ax);
            }
        }
        private void RadRadioButton_Rout2_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadioButton_Round2.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                Round = "2";
                this.GetDataBillToGrid(StatusGridView.Ax);
            }
        }
        private void RadRadioButton_Rout3_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radRadioButton_Round3.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                Round = "3";
                this.GetDataBillToGrid(StatusGridView.Ax);
            }
        }
        #endregion

        #region SplitButtonDept
        //Set DropDrowButton Dept
        private void SetButtonDept()
        {
            radSplitButton_Dept.DropDownButtonElement.Font = Controllers.Connections.FontTahoma12;
            this.ControlsDept(PrintQueryClass.GetDeptUseAndroidDept(Connections.SystemDept));
            foreach (RadMenuItem radMenuItem in radSplitButton_Dept.Items)
            {
                if (radMenuItem.Text.Substring(0, 4).Equals(Connections.SystemDept))
                    this.SetDefaultItem(radMenuItem);
            }
        }
        //Add Item In DropDrowButton Dept
        private void ControlsDept(DataTable _dt)
        {
            RadDropDownButton radDropDownButton = new RadDropDownButton();
            foreach (DataRow row in _dt.Rows)
            {
                RadMenuItem myRadMenuItem = new RadMenuItem()
                {
                    Text = row["DEPTID"].ToString() + " : " + row["DESCRIPTION"].ToString()
                };
                myRadMenuItem.Click += new EventHandler(MyRadMenuItem_Click);
                myRadMenuItem.Font = Controllers.Connections.FontTahoma12;
                radSplitButton_Dept.Items.Add(myRadMenuItem);
            }
            this.Controls.Add(radDropDownButton);
        }
        //abount DropDrowButton Dept
        private void MyRadMenuItem_Click(object sender, EventArgs e)
        {
            SetDefaultItem(sender as RadMenuItem);
            this.BindDropDownListGroupItem(PrintQueryClass.GetGroupItem(radSplitButton_Dept.Text.Substring(0, 4)));
            radCheckBox_GroupItem.Checked = false;
            radDropDownList_GroupItem.Enabled = false;
            GroupItem = "0";
            this.GetDataBillToGrid(StatusGridView.Ax);
        }
        //abount DropDrowButton Dept
        private void SetDefaultItem(RadMenuItem item)
        {
            radSplitButton_Dept.DefaultItem = item;
            radSplitButton_Dept.Text = item.Text; //item.Text.Substring(0, 4);
        }
        #endregion

        #region InitializeGrid
        private void InitializeGrid()
        {
            Property.SetDefaultRadGridView(RadGridView_Doc);
            Property.SetDefaultRadGridView(RadGridView_Apv);
            Property.SetDefaultRadGridView(RadGridView_Ax);

            List<COLUMNS> columnsDocument = new List<COLUMNS>() {
                new COLUMNS(){ TYPE = Property.ColumnTypes.Command,COLUMNNAME = "PRINTTAG", LABEL="TAG",WIDTH = 50,},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "DOCNO", LABEL="เลขที่เอกสาร",WIDTH = 140},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "DATEDOC", LABEL="วันที่เอกสาร",WIDTH = 140},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "BRANCHNAME", LABEL="สาขา",WIDTH = 150},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "STATUS", LABEL="สถานะ",WIDTH = 100,VISIBLE = false},
                //new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ITEMTYPE", LABEL="ประเภท",WIDTH = 100},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "COUNTBOX", LABEL="รวมลัง",WIDTH = 60},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "BOXPRINT", LABEL="ลังรอพิมพ์",WIDTH = 70},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ROUTEID", LABEL="สายรถ",WIDTH = 70},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ROUTENAME", LABEL="เส้นทาง",WIDTH = 150},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "WHOUP", LABEL="รหัสพนักงาน",WIDTH = 80}
            };
            columnsDocument.ForEach(c =>
            {
                RadGridView_Doc.Columns.Add(Property.AppendNewGridViewColumn(c.TYPE, c.COLUMNNAME, c.LABEL, c.WIDTH, c.VISIBLE));
                dt_Document.Columns.Add(c.COLUMNNAME);
            });

            List<COLUMNS> columnsApprove = new List<COLUMNS>() {
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "DOCNO", LABEL="เลขที่เอกสาร",WIDTH = 140},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "DATEDOC", LABEL="วันที่เอกสาร",WIDTH = 140},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "BRANCHNAME", LABEL="สาขา",WIDTH = 150},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "STATUS", LABEL="สถานะ",WIDTH = 100,VISIBLE = false},
                //new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ITEMTYPE", LABEL="ประเภท",WIDTH = 100},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "COUNTBOX", LABEL="รวมลัง",WIDTH = 60},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "BOXPRINT", LABEL="ลังรอพิมพ์",WIDTH = 70},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ROUTEID", LABEL="สายรถ",WIDTH = 70},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ROUTENAME", LABEL="เส้นทาง",WIDTH = 150},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "WHOUP", LABEL="รหัสพนักงาน",WIDTH = 80}
            };
            columnsApprove.ForEach(c =>
            {
                RadGridView_Apv.Columns.Add(Property.AppendNewGridViewColumn(c.TYPE, c.COLUMNNAME, c.LABEL, c.WIDTH, c.VISIBLE));
                dt_Approve.Columns.Add(c.COLUMNNAME);
            });

            List<COLUMNS> columnsAx = new List<COLUMNS>() {
                new COLUMNS(){ TYPE = Property.ColumnTypes.Command,COLUMNNAME = "PRINTTAG", LABEL="TAG",WIDTH = 50},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Command,COLUMNNAME = "PRINTBILL", LABEL="BILL",WIDTH = 50},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "DOCNO", LABEL="เลขที่เอกสาร",WIDTH = 140},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "DATEDOC", LABEL="วันที่เอกสาร",WIDTH = 120,VISIBLE = false},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "BRANCHNAME", LABEL="สาขา",WIDTH = 150},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "COUNTBOX", LABEL="รวมลัง",WIDTH = 60},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "STATUS", LABEL="สถานะ",WIDTH = 100,VISIBLE = false},
                //new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ITEMTYPE", LABEL="ประเภท",WIDTH = 100},
                //new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "COUNTBOX", LABEL="จำนวนลัง",WIDTH = 70},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ROUTEID", LABEL="สายรถ",WIDTH = 70},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "ROUTENAME", LABEL="เส้นทาง",WIDTH = 150},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "WHOUP", LABEL="ผู้อนุมัติ",WIDTH = 80},
            };
            columnsAx.ForEach(c =>
            {
                RadGridView_Ax.Columns.Add(Property.AppendNewGridViewColumn(c.TYPE, c.COLUMNNAME, c.LABEL, c.WIDTH, c.VISIBLE));
                dt_Ax.Columns.Add(c.COLUMNNAME);
            });
        }
        private void RadGridView_Ax_CellClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "PRINTTAG":
                    Timer1.Stop();
                    string DocNo = RadGridView_Ax.CurrentRow.Cells["DocNo"].Value.ToString();
                    this.Cursor = Cursors.WaitCursor;
                    DataTable dtTag = PrintQueryClass.GetBoxFaceSheet(DocNo);
                    if (dtTag.Rows.Count == 0)
                    {
                        MessageBox.Show($@"ไม่พบข้อมูลลังสินค้า บิล {DocNo} {Environment.NewLine}อยู่ในระหว่างการพักข้อมูล{Environment.NewLine}อีก 3 นาที ลองใหม่อีกครั้ง", Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Cursor = Cursors.Default;
                        Timer1.Start();
                        return;
                    }
                    this.Cursor = Cursors.Default;

                    using (TagPrint frmTagPrint = new TagPrint(DocNo, RadGridView_Ax.CurrentRow.Cells["BRANCHNAME"].Value.ToString(), "1", dtTag))
                    {
                        frmTagPrint.ShowDialog();
                    }
                    Timer1.Start();
                    break;
                case "PRINTBILL":
                    Timer1.Stop();
                    string _docNo = RadGridView_Ax.CurrentRow.Cells["DOCNO"].Value.ToString();
                    //PrintDocument(PrintQueryClass.GetAndroidPosTable(_docNo), PrintQueryClass.GetAndroidPosLine(_docNo), PrintQueryClass.GetBoxSheet(GetVoucherID(_docNo, Connections.SystemDept, "MRT"), ""));
                    this.Cursor = Cursors.WaitCursor;
                    PrintDocument(PrintQueryClass.GetAndroidPosTable(_docNo), PrintQueryClass.GetAndroidPosLine(_docNo), PrintQueryClass.GetBoxSheet(GetVoucherID(_docNo)));
                    this.Cursor = Cursors.Default;
                    Timer1.Start();
                    break;
            }
        }
        private void RadGridView_Doc_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column.Name.Equals("PRINTTAG"))
            {
                Timer1.Stop();
                if (Convert.ToInt32(RadGridView_Doc.CurrentRow.Cells["BOXPRINT"].Value) == 0 && Convert.ToInt32(RadGridView_Doc.CurrentRow.Cells["COUNTBOX"].Value) > 0)
                {
                    string DocNo = RadGridView_Doc.CurrentRow.Cells["DocNo"].Value.ToString();
                    //string DocMRT = $"MRT{DocNo.Substring(7, 6)}{DocNo.Substring(4,3)}-{DocNo.Substring(14, 4)}"; 
                    //string DocMRT = DocNo;
                    this.Cursor = Cursors.WaitCursor;
                    DataTable dtTag = PrintQueryClass.GetBoxFaceSheet(DocNo);
                    if (dtTag.Rows.Count == 0)
                    {
                        MessageBox.Show($@"ไม่พบข้อมูลลังสินค้า บิล {DocNo} {Environment.NewLine}อยู่ในระหว่างการพักข้อมูล{Environment.NewLine}อีก 3 นาที ลองใหม่อีกครั้ง", Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Cursor = Cursors.Default;
                        Timer1.Start();
                        return;
                    }
                    this.Cursor = Cursors.Default;
                    using (TagPrint frmTagPrint = new TagPrint(DocNo, RadGridView_Doc.CurrentRow.Cells["BranchName"].Value.ToString(), "0", dtTag))
                    {
                        //DialogResult dr = frmTagPrint.ShowDialog();
                        frmTagPrint.ShowDialog();
                    }
                }
                Timer1.Start();
            }
        }

        private void RadGridView_Doc_RowFormatting(object sender, RowFormattingEventArgs e)
        {
            if (e.RowElement.RowInfo != null) e.RowElement.ForeColor = Color.DarkGreen;
        }
        private void RadGridView_App_RowFormatting(object sender, RowFormattingEventArgs e)
        {
            if (e.RowElement.RowInfo != null) e.RowElement.ForeColor = Color.Chocolate;
        }
        private void RadGridView_Ax_RowFormatting(object sender, RowFormattingEventArgs e)
        {
            if (e.RowElement.RowInfo != null) e.RowElement.ForeColor = Color.DarkOrchid;
        }
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
            {
                e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
            }
            else
            {
                e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
            }
        }
        private void RadGridView_Ax_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (RadGridView_Ax.Rows.Count > 0)
            {
                //ToolTip
                if (e.ColumnIndex == RadGridView_Ax.Columns["PRINTTAG"].Index)
                {
                    if (e.Row is GridViewDataRowInfo) e.CellElement.ToolTipText = "พิมพ์ Tag";
                }
                else if (e.ColumnIndex == RadGridView_Ax.Columns["PRINTBILL"].Index)
                {
                    if (e.Row is GridViewDataRowInfo) e.CellElement.ToolTipText = "พิมพ์ Bill";
                }
                else
                {
                    if (e.Row is GridViewDataRowInfo)
                    {
                        var value = RadGridView_Ax.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                        e.CellElement.ToolTipText = value;
                    }
                }
            }
        }
        #endregion

        #region Method

        private void GetDataBillToGrid(StatusGridView _grid)
        {
            if (radSplitButton_Dept.Text == "แผนก") return;
            this.Cursor = Cursors.WaitCursor;
            string statusApv, statusAx;
            for (StatusGridView grid = StatusGridView.Document; grid <= _grid; grid++)
            {
                statusApv = grid == StatusGridView.Document ? "0" : "1";
                statusAx = grid == StatusGridView.Ax ? "1" : "0";
                this.LoadDataToGridview(PrintQueryClass.GetAndroidOrderToMain(documentDate, radSplitButton_Dept.Text.Substring(0, 4),
                    statusApv, statusAx, GroupItem, Round, grid), grid);
            }
            this.Cursor = Cursors.Default;
        }

        private void LoadDataToGridview(List<INVENTTRANSFER> _Lists, StatusGridView _statusDataGridView)
        {
            switch (_statusDataGridView)
            {
                case StatusGridView.Document:
                    if (dt_Document.Rows.Count > 0) { dt_Document.Rows.Clear(); dt_Document.AcceptChanges(); }

                    foreach (var list in _Lists)
                    {
                        DataRow row = dt_Document.NewRow();
                        string VoucherID = GetVoucherID(list.DOCNO);   //$"MRT{list.DOCNO.Substring(7, 6)}{list.DOCNO.Substring(4, 3)}-{list.DOCNO.Substring(14, 4)}";
                        row["PRINTTAG"] = list.PRINTTAG;
                        row["DOCNO"] = list.DOCNO;
                        row["DATEDOC"] = list.DATEDOC;
                        row["BRANCHNAME"] = list.BRANCHNAME;
                        row["STATUS"] = list.STATUS;
                        //row["ITEMTYPE"] = list.ITEMTYPE;
                        row["COUNTBOX"] = list.COUNTBOX;
                        row["BOXPRINT"] = list.BOXPRINT;
                        row["ROUTEID"] = list.ROUTEID;
                        row["ROUTENAME"] = list.ROUTENAME;
                        row["WHOUP"] = list.WHOUP;
                        dt_Document.Rows.Add(row);

                        if (Convert.ToInt32(list.BOXPRINT) > 0)
                        {
                            DOCUMENTMRTO DocumentMrto = new DOCUMENTMRTO()
                            {
                                DOCNO = list.DOCNO,
                                WHOUP = list.WHOUP,
                                ROUTEID = list.ROUTEID,
                                BRANCHNAME = list.BRANCHNAME,
                                COUNTBOX = Convert.ToInt32(list.COUNTBOX),
                                BOXPRINT = Convert.ToInt32(list.BOXPRINT)
                            };

                            string ret = InsertBox(VoucherID, DocumentMrto);
                            if (ret.Equals("0"))
                            {
                                DataTable dt_BoxFacesheet = PrintQueryClass.GetBoxFacesheetByVoucherid(VoucherID);
                                PrintQueryClass.PrintBoxFaceSheet30x40_GoDEX(Connections.PrintTag, dt_BoxFacesheet);

                                string retStatusPrint = PrintQueryClass.UpdateStatusPrint(VoucherID);
                                if (string.IsNullOrEmpty(retStatusPrint))
                                {
                                    row["BOXPRINT"] = "0";
                                    row["COUNTBOX"] = (DocumentMrto.COUNTBOX + DocumentMrto.BOXPRINT).ToString();
                                }
                                else
                                {
                                    MessageBox.Show(retStatusPrint, Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                MessageBox.Show(ret, Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }

                    RadGridView_Doc.DataSource = dt_Document;
                    break;
                case StatusGridView.Approve:
                    if (dt_Approve.Rows.Count > 0) { dt_Approve.Rows.Clear(); dt_Approve.AcceptChanges(); }
                    foreach (var list in _Lists)
                    {
                        DataRow row = dt_Approve.NewRow();
                        row["DOCNO"] = list.DOCNO;
                        row["DATEDOC"] = list.DATEDOC;
                        row["BRANCHNAME"] = list.BRANCHNAME;
                        row["STATUS"] = list.STATUS;
                        // row["ITEMTYPE"] = list.ITEMTYPE;
                        row["COUNTBOX"] = list.COUNTBOX;
                        row["BOXPRINT"] = list.BOXPRINT;
                        row["ROUTEID"] = list.ROUTEID;
                        row["ROUTENAME"] = list.ROUTENAME;
                        row["WHOUP"] = list.WHOAPV;
                        dt_Approve.Rows.Add(row);
                    }
                    RadGridView_Apv.DataSource = dt_Approve;
                    break;
                case StatusGridView.Ax:
                    if (dt_Ax.Rows.Count > 0) { dt_Ax.Rows.Clear(); dt_Ax.AcceptChanges(); }
                    foreach (var list in _Lists)
                    {
                        DataRow row = dt_Ax.NewRow();
                        row["PRINTTAG"] = list.PRINTTAG;
                        row["PRINTBILL"] = list.PRINTTAG;
                        row["DOCNO"] = list.DOCNO;
                        row["DATEDOC"] = list.DATEDOC;
                        row["BRANCHNAME"] = list.BRANCHNAME;
                        row["STATUS"] = list.STATUS;
                        //row["ITEMTYPE"] = list.ITEMTYPE;
                        row["COUNTBOX"] = list.COUNTBOX;
                        row["ROUTEID"] = list.ROUTEID;
                        row["ROUTENAME"] = list.ROUTENAME;
                        row["WHOUP"] = list.WHOAPV;
                        dt_Ax.Rows.Add(row);
                    }
                    RadGridView_Ax.DataSource = dt_Ax;
                    break;
                default:
                    break;
            }
        }
        private string GetVoucherID(string _docno)//, string _dept, string mrt = "MRT"
        {
            //mrt = _docno.Replace("O", ""); return mrt;
            return _docno.Replace("O", "");
        }
        private string GetRecId(string _docno, string _type, int _id)
        {
            return _type.Equals("") ? $"{_docno.Substring(7, 5)}{_docno.Substring(4, 2)}{_docno.Substring(13, 4)}{_id}" : $"{_type}{_docno.Substring(7, 5)}{_docno.Substring(4, 2)}{_docno.Substring(13, 4)}{_id}";
        }
        private void PrintDocument(ANDROIDPOSTOTABLE _posTable, DataTable _dt_posLine, string[] _groupBox)
        {
            //try
            //{
            reportViewer1.ProcessingMode = ProcessingMode.Local;
            reportViewer1.RefreshReport();

            BOXGROUP bOXGROUP = new BOXGROUP();
            for (int i = 0; i <= _groupBox.Length - 1; i++)
            {
                if (i == 0)
                {
                    bOXGROUP.BOXNAME = _groupBox[i].ToString();
                }
                else
                {
                    bOXGROUP.BOXNAME = bOXGROUP.BOXNAME + "," + _groupBox[i].ToString();
                }
                bOXGROUP.BOX = i + 1;
            }

            reportViewer1.LocalReport.DataSources.Clear();
            //reportViewer1.LocalReport.ReportEmbeddedResource = @"OrderToAx2022.Novat.rdlc";
            reportViewer1.LocalReport.ReportEmbeddedResource = @"OrderToAxPrint.Novat.rdlc";

            Microsoft.Reporting.WinForms.ReportDataSource dataset;
            dataset = new Microsoft.Reporting.WinForms.ReportDataSource("dsMapper_ORDERTOTO", _dt_posLine);
            reportViewer1.LocalReport.DataSources.Add(dataset);

            List<ReportParameter> Params = new List<ReportParameter>(){
                    new ReportParameter("P_INVENTLOCATIONIDTO", _posTable.INVENTLOCATIONIDTO),
                    new ReportParameter("P_INVENTLOCATIONIDFROM", _posTable.INVENTLOCATIONIDFROM),
                    new ReportParameter("P_VOUCHERID", _posTable.VOUCHERID),
                    new ReportParameter("P_TRANSFERID", _posTable.TRANSFERID),
                    new ReportParameter("P_CASHIERID", _posTable.CASHIERID),
                    new ReportParameter("P_SHIPDATE", _posTable.SHIPDATE),
                    new ReportParameter("P_ROUTESHIPMENTID", _posTable.ROUTESHIPMENTID),
                    new ReportParameter("P_SETDATETIME", _posTable.SETDATETIME),
                    new ReportParameter("P_BOX", bOXGROUP.BOX.ToString()),
                    new ReportParameter("P_BOXNAME", bOXGROUP.BOXNAME)
                };
            // set paramoter
            this.reportViewer1.LocalReport.SetParameters(Params);

            this.reportViewer1.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);
            reportViewer1.ShowParameterPrompts = false;

            reportViewer1.LocalReport.DisplayName = string.Format("พิมพ์เอกสาร");
            reportViewer1.LocalReport.Refresh();
            reportViewer1.RefreshReport();

            using (MyPrint _myPrint = new MyPrint())
            {
                _myPrint.Export(reportViewer1.LocalReport, Connections.PrintBill, "Bill");
                _myPrint.Print();
            }
        }
        private string InsertPosTable(int _frow)
        {
            if (RadGridView_Apv.Rows[Frow].Cells["Status"].Value.ToString().Equals("ส่งบิลเรียบร้อย")) return "0";

            else
            {
                string ret = "";
                DOCUMENTMRTO approveDocument = new DOCUMENTMRTO()
                {
                    DOCNO = RadGridView_Apv.Rows[_frow].Cells["DOCNO"].Value.ToString(),
                    WHOUP = RadGridView_Apv.Rows[_frow].Cells["WHOUP"].Value.ToString(),
                    DATEDOC = Convert.ToDateTime(RadGridView_Apv.Rows[_frow].Cells["DATEDOC"].Value).ToString("yyyy-MM-dd"),
                    ROUTEID = RadGridView_Apv.Rows[_frow].Cells["ROUTEID"].Value.ToString(),
                    BRANCHNAME = RadGridView_Apv.Rows[_frow].Cells["BRANCHNAME"].Value.ToString(),
                    COUNTBOX = Convert.ToInt32(RadGridView_Apv.Rows[_frow].Cells["COUNTBOX"].Value),
                    BOXPRINT = Convert.ToInt32(RadGridView_Apv.Rows[_frow].Cells["BOXPRINT"].Value)
                };

                List<ANDROIDORDER> androidOrderItemActive = PrintQueryClass.GetAndroidOrderCheckItemActive(approveDocument.DOCNO, Round);
                if (androidOrderItemActive.Count == 0)
                {
                    MessageBox.Show($"ไม่มีข้อมูลรายการสินค้า เอกสาร {approveDocument.DOCNO} ตามรอบที่เลือก กรุณาตรวจสอบในหน้าเอกสารจัดสินค้า", Connections.AppName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Timer1.Stop();
                }
                else
                {
                    string VoucherID = GetVoucherID(approveDocument.DOCNO);           //GetVoucherID
                    //string _dept = Connections.SystemDept;
                    //string wherehouseName = Connections.WherehouseName.Substring(11, Connections.WherehouseName.Length - 11);
                    //InsertAndroidPostoBoxtrans
                    if (Convert.ToInt32(RadGridView_Apv.Rows[_frow].Cells["BOXPRINT"].Value) > 0)
                    {
                        string retStatInsertBox = InsertBox(VoucherID, approveDocument);
                        if (retStatInsertBox.Equals("0")) approveDocument.BOXPRINT = 0;
                        else ret = "0";
                    }

                    if (approveDocument.BOXPRINT == 0)
                    {
                        string refId;
                        ArrayList arrayInsertAndroid = new ArrayList();
                        string sqlInsertAndroidTable = $@"
                        INSERT INTO ANDROID_POSTOTABLE 
                            (TRANSFERXLSID, FREIGHTSLIPTYPE, FREIGHTZONEID, REMARKS, CASHIERID
                            , POSGROUP, POSNUMBER, PICKINGROUTEID,ALLPRINTSTICKER, EMPLPICK
                            , EMPLCHECKER, ROUTESHIPMENTID, DLVTERM, DLVMODE, INVENTLOCATIONIDTO
                            , INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID, DATAAREAID
                            , RECVERSION, RecID, TRANSFERSTATUS, STADOC, STAAX
                            , STADPT,CREATEDATETIME)
                        VALUES 
                            ('', '98', '', '', '{approveDocument.WHOUP}'
                            ,'', '', '','1', '{approveDocument.WHOUP}'
                            ,'', '{approveDocument.ROUTEID}', '', '','{approveDocument.BRANCHNAME.ToString().Trim().Substring(0, 5)}'
                            ,'{Connections.WareHouseId}', '{approveDocument.DOCNO}','{approveDocument.DATEDOC}','{ VoucherID}', 'SPC'
                            ,'1', '1', '0', '1', '1'
                            ,'0',getdate()
                            )";
                        arrayInsertAndroid.Add(sqlInsertAndroidTable);

                        for (int r = 0; r <= androidOrderItemActive.Count - 1; r++)
                        {
                            refId = GetRecId(approveDocument.DOCNO, "92", 1);
                            string sqlInsertAndroidLine = $@"
                            INSERT INTO ANDROID_POSTOLINE
                                (VOUCHERID, SHIPDATE, LINENUM, QTY, SALESPRICE
                                ,ITEMID, NAME, SALESUNIT,ITEMBARCODE, INVENTSERIALID
                                ,INVENTDIMID, INVENTTRANSID, REFBOXTRANS, DATAAREAID, RECVERSION
                                ,RECID, INVENTBATCHID) 
                            VALUES     
                                ('{VoucherID}','{approveDocument.DATEDOC}','{r + 1}','{androidOrderItemActive[r].QTY}','{androidOrderItemActive[r].PRICE}'
                                ,'{androidOrderItemActive[r].ITEMID}','{androidOrderItemActive[r].NAME}',
                                '{androidOrderItemActive[r].UNITID}','{androidOrderItemActive[r].ITEMBARCODE}', ''
                                ,'{androidOrderItemActive[r].ITEMDIM}','{approveDocument.DOCNO.Substring(6, 11).Replace(" - ", "") + "_" + (r + 1).ToString()}','{refId}', 'SPC','1'
                                , '1','')";
                            arrayInsertAndroid.Add(sqlInsertAndroidLine);
                        }
                        string sqlUpdateAndroidToAx = $@"
                            UPDATE ANDROID_ORDERTOHD 
                            SET    StaAx='1' 
                                    ,DateUp=convert(varchar,getdate(),23)
                                    ,TimeUp=convert(varchar,getdate(),24)
                                    ,WhoUp='{approveDocument.WHOUP}'
                                    ,DateAx=convert(varchar,getdate(),23)
                                    ,TimeAx=convert(varchar,getdate(),24)
                                    ,WhoAx='{approveDocument.WHOUP}' 
                            WHERE Docno='{approveDocument.DOCNO}'";

                        arrayInsertAndroid.Add(sqlUpdateAndroidToAx);

                        ArrayList arrayInsertTmp = new ArrayList();
                        string sqlInsertTmpTable = $@"
                                INSERT INTO SPC_POSTOTABLE18
                                        (VOUCHERID, SHIPDATE, TRANSFERID, INVENTLOCATIONIDFROM, INVENTLOCATIONIDTO
                                        ,DLVMODE
                                        ,DLVTERM, ROUTESHIPMENTID,EMPLCHECKER, EMPLPICK, ALLPRINTSTICKER
                                        ,PICKINGROUTEID, DATAAREAID, RECVERSION, RECID
                                        ,POSNUMBER,POSGROUP, CASHIERID
                                        ,REMARKS, FREIGHTZONEID, FREIGHTSLIPTYPE
                                        ,TRANSFERXLSID, TRANSFERSTATUS
                                        ) 
                                VALUES
                                        ('{VoucherID}', Convert(varchar, getdate(), 23), '{VoucherID}','{Connections.WareHouseId}','{approveDocument.BRANCHNAME.Trim().Substring(0, 5)}'
                                        , ''
                                        , '', '','', '{approveDocument.WHOUP}', '1'
                                        , '','SPC','1', '1'
                                        , '','','{approveDocument.WHOUP}'
                                        , '','', '98'
                                        , '', '1')";

                        arrayInsertTmp.Add(sqlInsertTmpTable);

                        for (int r = 0; r <= androidOrderItemActive.Count - 1; r++)
                        {
                            refId = GetRecId(approveDocument.DOCNO, "92", 1);
                            if (androidOrderItemActive[r].QTY > 0)
                            {

                                string sqlInsertTmpLine = $@"
                                INSERT INTO SPC_POSTOLINE18
                                    (VOUCHERID, SHIPDATE, LINENUM, QTY, SALESPRICE  
                                    ,ITEMID, Name, SALESUNIT, ITEMBARCODE 
                                    ,INVENTSERIALID
                                    ,INVENTDIMID, INVENTTRANSID 
                                    ,REFBOXTRANS, DATAAREAID ,RECVERSION
                                    ,RecID, INVENTBATCHID) 
                                VALUES    
                                    ('{VoucherID}', Convert(varchar, getdate(), 23), '{r + 1}','{androidOrderItemActive[r].QTY}','{androidOrderItemActive[r].PRICE}'
                                    ,'{androidOrderItemActive[r].ITEMID}','{androidOrderItemActive[r].NAME}','{androidOrderItemActive[r].UNITID}','{androidOrderItemActive[r].ITEMBARCODE}'
                                    ,''
                                    ,'{androidOrderItemActive[r].ITEMDIM}','{VoucherID.Substring(3, 14) + "-" + (r + 1).ToString()}'
                                    ,'{refId}','SPC', '{approveDocument.DOCNO.Substring(13, 4) + r.ToString()}'
                                    , '1', '')";
                                arrayInsertTmp.Add(sqlInsertTmpLine);
                            }
                        }

                        var boxTransList = PrintQueryClass.GetBoxTrans(VoucherID);
                        var boxFacesheetList = PrintQueryClass.GetBoxFacesheet(VoucherID);
                        foreach (var boxTrans in boxTransList)
                        {
                            string sqlInsertTmpBoxtrans = $@"
                            INSERT INTO SPC_POSTOBOXTRANS18
                                    (VOUCHERID, BOXGROUP, NAME, BOXTYPE, BOXQTY
                                    , TRANSDATE,LINEWEIGHT, LINECAPACITY, ITEMBARCODE, REFBOXTRANS
                                    , DATAAREAID,RECVERSION, RecID, REFPALLETTRANS) 
                            VALUES
                                    ('{VoucherID}','006','{boxTrans.NAME}','{boxTrans.BOXTYPE}','{boxTrans.BOXQTY}'
                                    , convert(varchar,getdate(),23),'45000','45000', '', '{boxTrans.REFBOXTRANS}'
                                    , 'SPC','{boxTrans.ROW}','{boxTrans.RECID}','{boxTrans.REFPALLETTRANS}'
                                    )";
                            arrayInsertTmp.Add(sqlInsertTmpBoxtrans);
                        }

                        int _countBox = PrintQueryClass.GetCountBoxFaceSheet(VoucherID, "0");
                        int boxNumber = 1;
                        foreach (var boxFacesheet in boxFacesheetList)
                        {
                            string numlabel = boxNumber.ToString() + "/" + _countBox.ToString();
                            string sqlInsertTmpBoxface = $@" 
                            INSERT INTO SPC_POSTOBOXFACESHEET18
                                    (VOUCHERID, SHIPDATE, INVENTLOCATIONIDTO, LOCATIONNAMETO,BOXTYPE
                                    ,BOXNAME, DLVNAME, ROUTENAME, NUMBERLABEL, QTY 
                                    ,BoxGroup, REFBOXTRANS, DATAAREAID, RECVERSION, RecID
                                    ,SHIPPINGDATEREQUESTED) 
                            VALUES
                                    ('{VoucherID}', convert(varchar,getdate(),23),'{boxFacesheet.INVENTLOCATIONIDTO}','{boxFacesheet.LOCATIONNAMETO}','{boxFacesheet.BOXTYPE}'
                                    , '{boxFacesheet.BOXNAME}','{boxFacesheet.DLVNAME}', '{boxFacesheet.ROUTENAME.Replace(" ", ", ")}','{numlabel}', '1'
                                    , '{boxFacesheet.BOXGROUP}', '{boxFacesheet.REFBOXTRANS}', 'SPC', '{boxFacesheet.RECVERSION}', '{boxFacesheet.RECID}'
                                    ,convert(varchar,getdate(),25))";
                            arrayInsertTmp.Add(sqlInsertTmpBoxface);
                            boxNumber++;
                        }

                        string retInsertAx = ConnectionsDB.ExecuteSQL_Array2nd(arrayInsertAndroid, Connections.ConnMain, arrayInsertTmp, Connections.ConMainAX);
                        if (string.IsNullOrEmpty(retInsertAx))
                        {
                            ret = approveDocument.DOCNO;
                        }
                        else
                        {
                            MessageBox.Show(retInsertAx, Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ret = "0";
                        }
                    }
                }
                return ret;
            }
        }
        private string InsertBox(string voucherID, DOCUMENTMRTO documentMrto)
        {
            int countbox = documentMrto.COUNTBOX;
            ArrayList arraySql = new ArrayList();
            for (int r = 1; r <= documentMrto.BOXPRINT; r++)
            {
                string rec = GetRecId(documentMrto.DOCNO, "91", countbox + r);
                string refBox = GetRecId(documentMrto.DOCNO, "92", countbox + r);
                string boxGroup = GetRecId(documentMrto.DOCNO, "", countbox + r);
                string numlabel = r.ToString() + "/" + documentMrto.BOXPRINT.ToString();
                string sqlBoxtran = $@"
                            INSERT INTO [dbo].[ANDROID_POSTOBOXTRANS] 
                                (REFPALLETTRANS, REFBOXTRANS, ITEMBARCODE,LINECAPACITY, LINEWEIGHT
                                , TRANSDATE, BOXQTY,BOXTYPE, Name, BOXGROUP
                                , VOUCHERID,DATAAREAID, RECVERSION, RECID,SEQ) 
                            VALUES
                                ('0','{refBox}','0','45000','2.550'
                                ,convert(varchar,getdate(),23),'1','5','ลังสูง',''
                                ,'{voucherID}','SPC','0','{rec}',{countbox + r})";
                arraySql.Add(sqlBoxtran);

                string sqlBoxfacsheet = $@"
                            INSERT INTO [dbo].[ANDROID_POSTOBOXFACESHEET]
                                (SHIPPINGDATEREQUESTED, REFBOXTRANS, BOXGROUP, QTY, NUMBERLABEL
                                ,ROUTENAME, DLVNAME, BOXNAME, BOXTYPE, LOCATIONNAMETO
                                ,INVENTLOCATIONIDTO, SHIPDATE, VOUCHERID, DATAAREAID, RECVERSION
                                ,RECID,SEQ)
                            VALUES
                                (convert(varchar,getdate(),23),'{refBox}','{boxGroup}','1','{numlabel}'
                                ,'{documentMrto.ROUTEID}','','ลังสูง','5','{documentMrto.BRANCHNAME.Trim().Replace(" ", "").Substring(6, documentMrto.BRANCHNAME.Replace(" ", "").Length - 6)}'
                                ,'{documentMrto.BRANCHNAME.Trim().Replace(" ", "").Substring(0, 5)}',convert(varchar,getdate(),23),'{voucherID}','SPC','1'
                                ,'{rec}',{countbox + r})";
                arraySql.Add(sqlBoxfacsheet);
            }

            int boxTotal = documentMrto.COUNTBOX + documentMrto.BOXPRINT;
            string sqlUpdateBoxprint = $@"
                            UPDATE ANDROID_ORDERTOHD 
                            SET    BOX='{boxTotal}'
                                    ,BOXPRINT = '0'
                                    ,DateUp=convert(varchar,getdate(),23)
                                    ,TimeUp=convert(varchar,getdate(),24)
                                    ,WhoUp = '{documentMrto.WHOUP}'
                            WHERE Docno='{documentMrto.DOCNO}'";

            arraySql.Add(sqlUpdateBoxprint);

            string retExecute = ConnectionsDB.ExecuteSQL_ArrayMain(arraySql, Connections.ConnMain);
            if (string.IsNullOrEmpty(retExecute))
                return "0";
            else
                return retExecute;
        }
        #endregion

        #region ToolTip
        private void RadDropDownButton_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            if ((sender as ActionButtonElement) != null) e.ToolTipText = "แผนก";
            else if ((sender as RadArrowButtonElement) != null) e.ToolTipText = "เลือกแผนก";
        }

        #endregion

        private void MainPrint_Load_1(object sender, EventArgs e)
        {

        }
    }
}
