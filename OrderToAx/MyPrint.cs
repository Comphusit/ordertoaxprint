﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using Microsoft.Reporting.WinForms;
using System.Text;
//
using System.Drawing.Imaging;

namespace OrderToAx.Order
{
    public class MyPrint : IDisposable
    {
        private int m_currentPageIndex;
        private IList<Stream> m_streams;
        string selectedPrinterName = string.Empty;

        // Public Shared PrintTag As String = String.Empty
        // Public Shared PrintBill As String = String.Empty

        // Private Function LoadSalesData() As DataTable
        // ' Create a new DataSet and read sales data file 
        // ' data.xml into the first DataTable.
        // Dim dataSet As New DataSet()
        // dataSet.ReadXml("..\..\data.xml")
        // Return dataSet.Tables(0)
        // End Function

        // Routine to provide to the report renderer, in order to
        // save an image for each page of the report.
        private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }
        // Export the given report as an EMF (Enhanced Metafile) file.
        public void Export(LocalReport report, string Printer, string PaperSize)
        {
            string deviceInfo =
                 $@"<DeviceInfo>
                         <OutputFormat>EMF</OutputFormat>
                         <PageWidth>>3.0cm</PageWidth>
                         <PageHeight>>5.0cmin</PageHeight>
                         <MarginTop>0.0</MarginTop>
                         <MarginLeft>0.0cm</MarginLeft>
                         <MarginRight>0.0cm</MarginRight>
                         <MarginBottom>0.0cm</MarginBottom>
                     </DeviceInfo>";

            if (PaperSize.ToLower() == "bill")
                deviceInfo =
                    @"<DeviceInfo>
                        <OutputFormat>EMF</OutputFormat>
                        <PageWidth>21.0cm</PageWidth> 
                        <PageHeight>29.7cm</PageHeight> 
                        <MarginTop>0.0cm</MarginTop> 
                        <MarginLeft>0.0cm</MarginLeft> 
                        <MarginRight>0.0cm</MarginRight> 
                        <MarginBottom>14.97cm</MarginBottom> 
                        </DeviceInfo>";
            selectedPrinterName = Printer;

            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream,out Warning[] warnings);
            foreach (Stream stream in m_streams)
            {
                stream.Position = 0;
            }
        }

        // Handler for PrintPageEvents
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new Metafile(m_streams[m_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            Rectangle adjustedRect = new Rectangle(ev.PageBounds.Left - System.Convert.ToInt32(ev.PageSettings.HardMarginX), ev.PageBounds.Top - System.Convert.ToInt32(ev.PageSettings.HardMarginY), ev.PageBounds.Width, ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            m_currentPageIndex += 1;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        public void Print()
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();

            printDoc.PrinterSettings.PrinterName = selectedPrinterName;
            //printDoc.DefaultPageSettings.PrinterSettings.PrinterName = selectedPrinterName;

            if (!printDoc.PrinterSettings.IsValid)
                throw new Exception("Error: cannot find the default printer.");
            else
            {
                printDoc.PrintPage += PrintPage;

                m_currentPageIndex = 0;
                printDoc.Print();
            }
        }
        // Create a local report for Report.rdlc, load the data,
        // export the report to an .emf file, and print it.
        // Private Sub Run()
        // Dim report As New LocalReport()
        // 'report.ReportPath = "..\..\Report.rdlc"
        // 'report.DataSources.Add(New ReportDataSource("Sales", LoadSalesData()))
        // Export(report)
        // Print()
        // End Sub
        public void Dispose()
        {
            if (m_streams != null)
            {
                foreach (Stream stream in m_streams)
                {
                    stream.Close();
                }
                m_streams = null;
            }
        }
    }
}
