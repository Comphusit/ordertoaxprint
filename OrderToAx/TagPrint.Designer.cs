﻿namespace OrderToAx.Order
{
    partial class TagPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagPrint));
            this.radButton_Print = new Telerik.WinControls.UI.RadButton();
            this.PanelContainer = new Telerik.WinControls.UI.RadScrollablePanelContainer();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radButton_DontSelect = new Telerik.WinControls.UI.RadButton();
            this.radButton_SelectAll = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Branch = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Mrto = new Telerik.WinControls.UI.RadLabel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridView_Detail = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Print)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_DontSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Mrto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Detail.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Print
            // 
            this.radButton_Print.BackColor = System.Drawing.Color.SkyBlue;
            this.radButton_Print.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_Print.Location = new System.Drawing.Point(327, 106);
            this.radButton_Print.Name = "radButton_Print";
            this.radButton_Print.Size = new System.Drawing.Size(130, 32);
            this.radButton_Print.TabIndex = 4;
            this.radButton_Print.Text = "พิมพ์";
            this.radButton_Print.ThemeName = "Fluent";
            this.radButton_Print.Click += new System.EventHandler(this.RadButton_Print_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Print.GetChildAt(0))).Text = "พิมพ์";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Print.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Print.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Print.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Print.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Print.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Print.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Print.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Print.GetChildAt(0).GetChildAt(0))).AutoSize = false;
            // 
            // PanelContainer
            // 
            this.PanelContainer.AutoScroll = false;
            this.PanelContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PanelContainer.Dock = System.Windows.Forms.DockStyle.None;
            this.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.PanelContainer.Size = new System.Drawing.Size(198, 98);
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(469, 601);
            this.radSplitContainer1.TabIndex = 5;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radLabel_Date);
            this.splitPanel1.Controls.Add(this.radButton_DontSelect);
            this.splitPanel1.Controls.Add(this.radButton_SelectAll);
            this.splitPanel1.Controls.Add(this.radLabel_Branch);
            this.splitPanel1.Controls.Add(this.radLabel_Mrto);
            this.splitPanel1.Controls.Add(this.radButton_Print);
            this.splitPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(469, 146);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.256135F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -129);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.AutoSize = false;
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Date.Location = new System.Drawing.Point(12, 76);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(445, 23);
            this.radLabel_Date.TabIndex = 103;
            this.radLabel_Date.Text = "วันที่";
            // 
            // radButton_DontSelect
            // 
            this.radButton_DontSelect.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_DontSelect.Location = new System.Drawing.Point(135, 106);
            this.radButton_DontSelect.Name = "radButton_DontSelect";
            this.radButton_DontSelect.Size = new System.Drawing.Size(117, 32);
            this.radButton_DontSelect.TabIndex = 102;
            this.radButton_DontSelect.Text = "ยกเลิก";
            this.radButton_DontSelect.ThemeName = "Fluent";
            this.radButton_DontSelect.Click += new System.EventHandler(this.RadButton_DontSelect_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_DontSelect.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_DontSelect.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_DontSelect.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_DontSelect.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_DontSelect.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_DontSelect.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_DontSelect.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_DontSelect.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_SelectAll
            // 
            this.radButton_SelectAll.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radButton_SelectAll.Location = new System.Drawing.Point(12, 106);
            this.radButton_SelectAll.Name = "radButton_SelectAll";
            this.radButton_SelectAll.Size = new System.Drawing.Size(117, 32);
            this.radButton_SelectAll.TabIndex = 101;
            this.radButton_SelectAll.Text = "เลือกทั้งหมด";
            this.radButton_SelectAll.ThemeName = "Fluent";
            this.radButton_SelectAll.Click += new System.EventHandler(this.RadButton_SelectAll_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_SelectAll.GetChildAt(0))).Text = "เลือกทั้งหมด";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_SelectAll.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_SelectAll.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SelectAll.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SelectAll.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SelectAll.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SelectAll.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_SelectAll.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_Branch
            // 
            this.radLabel_Branch.AutoSize = false;
            this.radLabel_Branch.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Branch.Location = new System.Drawing.Point(12, 12);
            this.radLabel_Branch.Name = "radLabel_Branch";
            this.radLabel_Branch.Size = new System.Drawing.Size(445, 23);
            this.radLabel_Branch.TabIndex = 99;
            this.radLabel_Branch.Text = "สาขา";
            // 
            // radLabel_Mrto
            // 
            this.radLabel_Mrto.AutoSize = false;
            this.radLabel_Mrto.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Mrto.Location = new System.Drawing.Point(12, 45);
            this.radLabel_Mrto.Name = "radLabel_Mrto";
            this.radLabel_Mrto.Size = new System.Drawing.Size(445, 23);
            this.radLabel_Mrto.TabIndex = 98;
            this.radLabel_Mrto.Text = "MRTO";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radGridView_Detail);
            this.splitPanel2.Location = new System.Drawing.Point(0, 150);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(469, 451);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.256135F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 129);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radGridView_Detail
            // 
            this.radGridView_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Detail.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView_Detail.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Detail.Name = "radGridView_Detail";
            this.radGridView_Detail.Size = new System.Drawing.Size(469, 451);
            this.radGridView_Detail.TabIndex = 0;
            this.radGridView_Detail.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.RadGridView_Detail_RowFormatting);
            this.radGridView_Detail.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Detail_ViewCellFormatting);
            this.radGridView_Detail.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Detail_CellClick);
            // 
            // TagPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(469, 601);
            this.Controls.Add(this.radSplitContainer1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagPrint";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "เลือก TAG เพื่อพิมพ์ซ้ำ";
            this.Load += new System.EventHandler(this.TagPrint_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Print)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_DontSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Mrto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Detail.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Print;
        private Telerik.WinControls.UI.RadScrollablePanelContainer PanelContainer;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView radGridView_Detail;
        private Telerik.WinControls.UI.RadLabel radLabel_Mrto;
        private Telerik.WinControls.UI.RadLabel radLabel_Branch;
        protected Telerik.WinControls.UI.RadButton radButton_DontSelect;
        protected Telerik.WinControls.UI.RadButton radButton_SelectAll;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
    }
}
