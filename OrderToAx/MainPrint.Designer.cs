﻿namespace OrderToAx.Order
{
    partial class MainPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPrint));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RadGridView_Apv = new Telerik.WinControls.UI.RadGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RadGridView_Doc = new Telerik.WinControls.UI.RadGridView();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radSplitButton_Dept = new Telerik.WinControls.UI.RadSplitButton();
            this.radCheckBox_GroupItem = new Telerik.WinControls.UI.RadCheckBox();
            this.radDateTimePicker_Date = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radRadioButton_Round3 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Round2 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton_Round1 = new Telerik.WinControls.UI.RadRadioButton();
            this.radDropDownList_GroupItem = new Telerik.WinControls.UI.RadDropDownList();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RadGridView_Ax = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer_Main = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel6 = new Telerik.WinControls.UI.SplitPanel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Apv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Apv.MasterTemplate)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Doc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Doc.MasterTemplate)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitButton_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_GroupItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupItem)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Ax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Ax.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer_Main)).BeginInit();
            this.radSplitContainer_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1162, 653);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Controls.Add(this.radSplitContainer_Main);
            this.tabPage1.Controls.Add(this.radSplitContainer3);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1154, 624);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "เอกสาร";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(1148, 618);
            this.splitContainer1.SplitterDistance = 656;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(656, 618);
            this.splitContainer2.SplitterDistance = 315;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RadGridView_Apv);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(656, 315);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "กำลังประมวลผล [รอพิมพ์]";
            // 
            // RadGridView_Apv
            // 
            this.RadGridView_Apv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Apv.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Apv.Location = new System.Drawing.Point(3, 19);
            // 
            // 
            // 
            this.RadGridView_Apv.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Apv.Name = "RadGridView_Apv";
            this.RadGridView_Apv.Size = new System.Drawing.Size(650, 293);
            this.RadGridView_Apv.TabIndex = 0;
            this.RadGridView_Apv.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.RadGridView_App_RowFormatting);
            this.RadGridView_Apv.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RadGridView_Doc);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(656, 299);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "รออนุมัติ [กำลังจัดบิล]";
            // 
            // RadGridView_Doc
            // 
            this.RadGridView_Doc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Doc.Location = new System.Drawing.Point(3, 19);
            // 
            // 
            // 
            this.RadGridView_Doc.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_Doc.Name = "RadGridView_Doc";
            this.RadGridView_Doc.Size = new System.Drawing.Size(650, 277);
            this.RadGridView_Doc.TabIndex = 1;
            this.RadGridView_Doc.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.RadGridView_Doc_RowFormatting);
            this.RadGridView_Doc.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Doc.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Doc_CellClick);
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer3.Size = new System.Drawing.Size(491, 618);
            this.splitContainer3.SplitterDistance = 88;
            this.splitContainer3.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.radSplitButton_Dept);
            this.panel1.Controls.Add(this.radCheckBox_GroupItem);
            this.panel1.Controls.Add(this.radDateTimePicker_Date);
            this.panel1.Controls.Add(this.radRadioButton_Round3);
            this.panel1.Controls.Add(this.radRadioButton_Round2);
            this.panel1.Controls.Add(this.radRadioButton_Round1);
            this.panel1.Controls.Add(this.radDropDownList_GroupItem);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(489, 86);
            this.panel1.TabIndex = 1;
            // 
            // radSplitButton_Dept
            // 
            this.radSplitButton_Dept.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radSplitButton_Dept.Location = new System.Drawing.Point(5, 49);
            this.radSplitButton_Dept.Name = "radSplitButton_Dept";
            this.radSplitButton_Dept.Size = new System.Drawing.Size(208, 33);
            this.radSplitButton_Dept.TabIndex = 2;
            this.radSplitButton_Dept.Text = "แผนก";
            this.radSplitButton_Dept.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.RadDropDownButton_ToolTipTextNeeded);
            // 
            // radCheckBox_GroupItem
            // 
            this.radCheckBox_GroupItem.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_GroupItem.Location = new System.Drawing.Point(219, 54);
            this.radCheckBox_GroupItem.Name = "radCheckBox_GroupItem";
            this.radCheckBox_GroupItem.Size = new System.Drawing.Size(55, 23);
            this.radCheckBox_GroupItem.TabIndex = 106;
            this.radCheckBox_GroupItem.Text = "กลุ่ม";
            this.radCheckBox_GroupItem.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_GroupItem_ToggleStateChanged);
            // 
            // radDateTimePicker_Date
            // 
            this.radDateTimePicker_Date.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_Date.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radDateTimePicker_Date.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radDateTimePicker_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.radDateTimePicker_Date.Location = new System.Drawing.Point(320, 12);
            this.radDateTimePicker_Date.Name = "radDateTimePicker_Date";
            this.radDateTimePicker_Date.Size = new System.Drawing.Size(153, 25);
            this.radDateTimePicker_Date.TabIndex = 103;
            this.radDateTimePicker_Date.TabStop = false;
            this.radDateTimePicker_Date.Text = "04/06/2022";
            this.radDateTimePicker_Date.Value = new System.DateTime(2022, 6, 4, 0, 0, 0, 0);
            this.radDateTimePicker_Date.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Date_ValueChanged);
            // 
            // radRadioButton_Round3
            // 
            this.radRadioButton_Round3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Round3.Location = new System.Drawing.Point(176, 14);
            this.radRadioButton_Round3.Name = "radRadioButton_Round3";
            this.radRadioButton_Round3.Size = new System.Drawing.Size(128, 23);
            this.radRadioButton_Round3.TabIndex = 102;
            this.radRadioButton_Round3.TabStop = false;
            this.radRadioButton_Round3.Text = "รอบที่ 1 และ 2";
            this.radRadioButton_Round3.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Rout3_ToggleStateChanged);
            // 
            // radRadioButton_Round2
            // 
            this.radRadioButton_Round2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Round2.Location = new System.Drawing.Point(91, 14);
            this.radRadioButton_Round2.Name = "radRadioButton_Round2";
            this.radRadioButton_Round2.Size = new System.Drawing.Size(79, 23);
            this.radRadioButton_Round2.TabIndex = 101;
            this.radRadioButton_Round2.TabStop = false;
            this.radRadioButton_Round2.Text = "รอบที่ 2";
            this.radRadioButton_Round2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Rout2_ToggleStateChanged);
            // 
            // radRadioButton_Round1
            // 
            this.radRadioButton_Round1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton_Round1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radRadioButton_Round1.Location = new System.Drawing.Point(5, 14);
            this.radRadioButton_Round1.Name = "radRadioButton_Round1";
            this.radRadioButton_Round1.Size = new System.Drawing.Size(79, 23);
            this.radRadioButton_Round1.TabIndex = 100;
            this.radRadioButton_Round1.Text = "รอบที่ 1";
            this.radRadioButton_Round1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radRadioButton_Round1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadRadioButton_Rout1_ToggleStateChanged);
            // 
            // radDropDownList_GroupItem
            // 
            this.radDropDownList_GroupItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radDropDownList_GroupItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_GroupItem.AutoSize = false;
            this.radDropDownList_GroupItem.BackColor = System.Drawing.Color.White;
            this.radDropDownList_GroupItem.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_GroupItem.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radDropDownList_GroupItem.Location = new System.Drawing.Point(280, 51);
            this.radDropDownList_GroupItem.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_GroupItem.Name = "radDropDownList_GroupItem";
            // 
            // 
            // 
            this.radDropDownList_GroupItem.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_GroupItem.Size = new System.Drawing.Size(204, 28);
            this.radDropDownList_GroupItem.TabIndex = 96;
            this.radDropDownList_GroupItem.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.RadDropDownList_GroupItem_SelectedIndexChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_GroupItem.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_GroupItem.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_GroupItem.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RadGridView_Ax);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(489, 524);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ส่งข้อมูลเรียบร้อยแล้ว [เข้า AX แล้ว]";
            // 
            // RadGridView_Ax
            // 
            this.RadGridView_Ax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Ax.Location = new System.Drawing.Point(3, 19);
            // 
            // 
            // 
            this.RadGridView_Ax.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.RadGridView_Ax.Name = "RadGridView_Ax";
            this.RadGridView_Ax.Size = new System.Drawing.Size(483, 502);
            this.RadGridView_Ax.TabIndex = 1;
            this.RadGridView_Ax.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.RadGridView_Ax_RowFormatting);
            this.RadGridView_Ax.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Ax_CellFormatting);
            this.RadGridView_Ax.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Ax.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Ax_CellClick);
            // 
            // radSplitContainer_Main
            // 
            this.radSplitContainer_Main.Controls.Add(this.splitPanel1);
            this.radSplitContainer_Main.Controls.Add(this.splitPanel2);
            this.radSplitContainer_Main.Location = new System.Drawing.Point(5, 437);
            this.radSplitContainer_Main.Name = "radSplitContainer_Main";
            // 
            // 
            // 
            this.radSplitContainer_Main.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer_Main.Size = new System.Drawing.Size(109, 53);
            this.radSplitContainer_Main.TabIndex = 1;
            this.radSplitContainer_Main.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radSplitContainer2);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(57, 53);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.04450262F, 0F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(64, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 96);
            this.radSplitContainer2.Name = "radSplitContainer2";
            this.radSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(328, 266);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(328, 149);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.06703911F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 39);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // splitPanel4
            // 
            this.splitPanel4.Location = new System.Drawing.Point(0, 153);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(328, 113);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.06703911F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -39);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Location = new System.Drawing.Point(61, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(48, 53);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.04450262F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(-64, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel5);
            this.radSplitContainer3.Controls.Add(this.splitPanel6);
            this.radSplitContainer3.Location = new System.Drawing.Point(133, 394);
            this.radSplitContainer3.Name = "radSplitContainer3";
            this.radSplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer3.Size = new System.Drawing.Size(412, 96);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            // 
            // splitPanel5
            // 
            this.splitPanel5.Location = new System.Drawing.Point(0, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel5.Size = new System.Drawing.Size(412, 30);
            this.splitPanel5.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.176525F);
            this.splitPanel5.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -91);
            this.splitPanel5.TabIndex = 0;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // splitPanel6
            // 
            this.splitPanel6.Location = new System.Drawing.Point(0, 34);
            this.splitPanel6.Name = "splitPanel6";
            // 
            // 
            // 
            this.splitPanel6.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel6.Size = new System.Drawing.Size(412, 62);
            this.splitPanel6.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.176525F);
            this.splitPanel6.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 91);
            this.splitPanel6.TabIndex = 1;
            this.splitPanel6.TabStop = false;
            this.splitPanel6.Text = "splitPanel6";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.reportViewer1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1154, 624);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "พิมพ์";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Location = new System.Drawing.Point(3, 3);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1148, 618);
            this.reportViewer1.TabIndex = 0;
            // 
            // MainPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1162, 653);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainPrint";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderToAXPrint";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainPrint_Load_1);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Apv.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Apv)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Doc.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Doc)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitButton_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_GroupItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton_Round1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupItem)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Ax.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Ax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer_Main)).EndInit();
            this.radSplitContainer_Main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Date;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Round3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Round2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton_Round1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_GroupItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private Telerik.WinControls.UI.RadGridView RadGridView_Ax;
        private System.Windows.Forms.GroupBox groupBox2;
        private Telerik.WinControls.UI.RadGridView RadGridView_Doc;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer_Main;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private Telerik.WinControls.UI.RadGridView RadGridView_Apv;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.SplitPanel splitPanel6;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_GroupItem;
        private System.Windows.Forms.Timer Timer1;
        private Telerik.WinControls.UI.RadSplitButton radSplitButton_Dept;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
    }
}
