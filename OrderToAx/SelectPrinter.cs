﻿using System;
using System.Windows.Forms;
using System.Data;
using OrderToAx.Order.Controllers;
using System.Drawing;
using System.Diagnostics;

namespace OrderToAx.Order
{
    public partial class SelectPrinter : Telerik.WinControls.UI.RadForm
    {
        public SelectPrinter()
        {
            InitializeComponent();
            this.InitializeFrom();
        }
        void InitializeFrom()
        {
            this.Load += new EventHandler(this.SelectPrinter_Load);
        }
        private void SelectPrinter_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt16(Connections.SystemVersion) < Convert.ToInt16(GetVersion()))
            {
                MessageBox.Show($@"ไม่สามารถใช้งานโปรแกรมได้{Environment.NewLine}เนื่องจากเวอร์ชั่นไม่ Update{Environment.NewLine}ให้ Update โปรแกรม ตาม Link ที่เปิดไว้ก่อนใช้งาน.",
                   Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                ProcessStartInfo startInfo = new ProcessStartInfo("IExplore.exe")
                {
                    WindowStyle = ProcessWindowStyle.Maximized,
                    Arguments = "http://spc310srv/PublishApps/OrderToAxPrint/OrderToAxPrint.htm"

                };
                Process.Start(startInfo);
                Application.Exit();
            }

            Property.SetFontDropDown(radDropDownList_Location, Connections.FontTahoma12, Color.Blue);
            Property.SetFontDropDown(radDropDownList_PrintTag, Connections.FontTahoma12, Color.Blue, 350, 0);
            Property.SetFontDropDown(radDropDownList_PrintBill, Connections.FontTahoma12, Color.Blue, 350, 0);

            this.SetPrint();
            this.SetLocation(GetLocation());
            //this.Text = GetVersion();

        }
        //VERSION
        string GetVersion()
        {
            string sql = string.Format(@"
                    SELECT	USERNAME AS VERSION_USE	
                    FROM	SHOP_CONFIGDB WITH (NOLOCK)
                    WHERE	VARIABLENAME = 'SystemVersionOrderToAXPrint'
                    ", Controllers.Connections.ConnMain);
            return Controllers.ConnectionsDB.GetSelectSQL(sql).Rows[0]["VERSION_USE"].ToString();
        }
        //WH
        DataTable GetLocation()
        {
            string sql = string.Format(@"
                    SELECT  INVENTLOCATIONID,INVENTLOCATIONID +' '+ NAME AS INVENTLOCATIONNAME 
                    FROM    SHOP2013TMP.dbo.INVENTLOCATION WITH (NOLOCK) 
                    WHERE   INVENTLOCATIONID  IN ('RETAILAREA'  ,'WH-A') 
                    ORDER BY NAME ASC
                    ", Controllers.Connections.ConnMain);
            return Controllers.ConnectionsDB.GetSelectSQL(sql);
        }

        //static internal string GetVersion()
        //{
        //    if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
        //    {
        //        return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
        //    }
        //    else
        //    {
        //        return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        //    }
        //}
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        private void RadButton_Select_Click(object sender, EventArgs e)
        {
            if (radDropDownList_PrintTag.SelectedItem == null || string.IsNullOrEmpty(radDropDownList_PrintTag.SelectedItem.Text))
            {
                MessageBox.Show("ต้องเลือกเครื่องพิมพ์ TAG ก่อนการใช้งาน", Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                radDropDownList_PrintTag.Focus();
                return;
            }

            if (radDropDownList_PrintBill.SelectedItem == null || string.IsNullOrEmpty(radDropDownList_PrintBill.SelectedItem.Text))
            {
                MessageBox.Show("ต้องเลือกเครื่องพิมพ์ BILL ก่อนการใช้งาน", Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                radDropDownList_PrintBill.Focus();
                return;
            }

            Connections.WareHouseId = radDropDownList_Location.SelectedValue.ToString();
            Connections.WareHouseName = radDropDownList_Location.Text.ToString();

            Connections.PrintTag = radDropDownList_PrintTag.SelectedItem.ToString();
            Connections.PrintBill = radDropDownList_PrintBill.SelectedItem.ToString();

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        #region Setup Printer 
        private void SetPrint()
        {
            try
            {
                foreach (var printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                {
                    radDropDownList_PrintTag.Items.Add(printer.ToString());
                    radDropDownList_PrintBill.Items.Add(printer.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ไม่สามารถค้นหา Printer ได้ ลองใหม่อีกครั้ง" + Environment.NewLine + ex.Message, Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }
        #endregion
        private void SetLocation(DataTable _dt)
        {
            radDropDownList_Location.DataSource = _dt;
            radDropDownList_Location.DisplayMember = "INVENTLOCATIONNAME";
            radDropDownList_Location.ValueMember = "INVENTLOCATIONID";
        }

        private void SelectPrinter_Load_1(object sender, EventArgs e)
        {

        }
    }
}
