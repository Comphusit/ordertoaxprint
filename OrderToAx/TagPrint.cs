﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using OrderToAx.Order.Controllers;
using OrderToAx.Order.Table;

namespace OrderToAx.Order
{
    public partial class TagPrint : Telerik.WinControls.UI.RadForm
    {
        #region Fields 
        readonly string _DocNo, _Branch, _Status;// _MRT,
        readonly DataTable dt;
        // DataTable dt = new DataTable();
        #endregion

        #region InitializeForm
        public TagPrint(string DocNo, string Branch, string Status, DataTable dtDetail)
        {
            InitializeComponent();
            _DocNo = DocNo;
            _Branch = Branch;
            //_MRT = mrt;
            _Status = Status;
            dt = dtDetail;
        }
        private void TagPrint_Load(object sender, EventArgs e)
        {
            radLabel_Mrto.Text = _DocNo;
            radLabel_Branch.Text = _Branch;

            Property.SetDefaultRadGridView(radGridView_Detail);

            this.BindDataGridview();
        }
        #endregion

        #region Gridview
        private void BindDataGridview()
        {
            List<COLUMNS> columns = new List<COLUMNS>() {
                new COLUMNS(){ TYPE = Property.ColumnTypes.CheckBox,COLUMNNAME = "CHOOSE", LABEL="เลือก",WIDTH = 70},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "BOXGROUP", LABEL="เลขที่ TAG",WIDTH = 150},
                new COLUMNS(){ TYPE = Property.ColumnTypes.Text,COLUMNNAME = "NUMBERLABEL", LABEL="TAG",WIDTH = 70},
            };
            columns.ForEach(c =>
            {
                radGridView_Detail.Columns.Add(Property.AppendNewGridViewColumn(c.TYPE, c.COLUMNNAME, c.LABEL, c.WIDTH, c.VISIBLE));
            });
 
            radGridView_Detail.DataSource = dt;
            radLabel_Date.Text = dt.Rows[0]["SHIPDATE"].ToString();
        }
       
        #endregion


        #region Button
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        private void RadButton_Print_Click(object sender, EventArgs e)
        {
            if (radGridView_Detail.Rows.Count > 0)
            {
                DataTable dt_DetailSelect = new DataTable();
                dt_DetailSelect.Columns.Add("VOUCHERID");
                dt_DetailSelect.Columns.Add("NUMBERLABEL");
                dt_DetailSelect.Columns.Add("BOXNAME");
                dt_DetailSelect.Columns.Add("BOXGROUP");
                dt_DetailSelect.Columns.Add("SHIPDATE");
                dt_DetailSelect.Columns.Add("ROUTENAME");
                dt_DetailSelect.Columns.Add("LOCATIONNAMETO");

                foreach (var row in dt.Select("CHOOSE = '1'"))
                {
                    DataRow newRow = dt_DetailSelect.NewRow();
                    newRow["VOUCHERID"] = row["VOUCHERID"].ToString();
                    newRow["NUMBERLABEL"] = row["NUMBERLABEL"].ToString();
                    newRow["BOXNAME"] = row["BOXNAME"].ToString();
                    newRow["BOXGROUP"] = row["BOXGROUP"].ToString();
                    newRow["SHIPDATE"] = row["SHIPDATE"].ToString();
                    newRow["ROUTENAME"] = row["ROUTENAME"].ToString();
                    newRow["LOCATIONNAMETO"] = row["LOCATIONNAMETO"].ToString();
                    dt_DetailSelect.Rows.Add(newRow);
                }

                if (dt_DetailSelect.Rows.Count == 0)
                {
                    MessageBox.Show("กรุณาเลือกรายการก่อนพิมพ์.", Connections.AppName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                PrintQueryClass.PrintBoxFaceSheet30x40_GoDEX(Connections.PrintTag, dt_DetailSelect);
            }
        }
        private void RadButton_SelectAll_Click(object sender, EventArgs e)
        {           
            foreach (DataRow r in this.dt.Rows)
            {
                r["CHOOSE"] = "1";
            }
            dt.AcceptChanges();
            radGridView_Detail.DataSource = dt;
        }
        private void RadButton_DontSelect_Click(object sender, EventArgs e)
        {
            foreach (DataRow r in this.dt.Rows)
            {
                r["CHOOSE"] = "0";
            }
            dt.AcceptChanges();
            radGridView_Detail.DataSource = dt;
        }
        #endregion

        #region GridView
        private void RadGridView_Detail_RowFormatting(object sender, Telerik.WinControls.UI.RowFormattingEventArgs e)
        {
            if (_Status.Equals("1"))
            {
                e.RowElement.ForeColor = Color.DarkOrchid;
            }
            else
            {
                e.RowElement.ForeColor = Color.DarkGreen;
                e.RowElement.ResetValue(LightVisualElement.BackColorProperty, Telerik.WinControls.ValueResetFlags.Local);
                e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, Telerik.WinControls.ValueResetFlags.Local);
                e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, Telerik.WinControls.ValueResetFlags.Local);
            }
        }
        private void RadGridView_Detail_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
            {
                e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
            }
            else
            {
                e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
            }
        }
        private void RadGridView_Detail_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.Index > -1 && e.Column.Name.Equals("CHOOSE"))
            {
                if (dt.Rows[e.RowIndex]["CHOOSE"].ToString().Equals("0")) dt.Rows[e.RowIndex]["CHOOSE"] = "1";
                else dt.Rows[e.RowIndex]["CHOOSE"] = "0";
            }


            dt.AcceptChanges();
            radGridView_Detail.DataSource = dt;
        }
        #endregion
    }
}
