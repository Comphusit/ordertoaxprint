﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace OrderToAx
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
            string strProcessName = Process.GetCurrentProcess().ProcessName;

            Process[] Oprocess = Process.GetProcessesByName(strProcessName);

            if (Oprocess.Length > 1)
            {
                MessageBox.Show("โปรแกรมกำลังเปิดใช้งานอยู่...");
                return;
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //Application.Run(new Form1());
                Application.Run(new Order.MainPrint());
            }
        }
    }
}
