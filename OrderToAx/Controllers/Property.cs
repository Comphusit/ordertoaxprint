﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace OrderToAx.Order.Controllers
{
    class Property
    {
        public enum ColumnTypes
        {
            Text = 0,
            NumericText = 1,
            Decimal = 2,
            DateTime = 3,
            Image = 4,
            Command = 5,
            CheckBox = 6
        }
        public static void SetFontDropDown(RadDropDownList rad_Dropdown, Font _font, Color _foreColor, int _dropDownWidth = 0, int _dropDownHeight = 0)
        {
            rad_Dropdown.DropDownListElement.ListElement.Font = _font;
            rad_Dropdown.DropDownListElement.Font = _font;
            rad_Dropdown.DropDownListElement.ListElement.ForeColor = _foreColor;
            rad_Dropdown.DropDownListElement.TextBox.Font = _font;
            rad_Dropdown.DropDownListElement.TextBox.ForeColor = _foreColor;
            rad_Dropdown.DropDownListElement.DropDownWidth = _dropDownWidth != 0 ? _dropDownWidth : 120;   //120
            rad_Dropdown.DropDownListElement.DropDownHeight = _dropDownHeight != 0 ? _dropDownHeight : 180;  //180
            //rad_Dropdown.DropDownSizingMode = SizingMode.UpDownAndRightBottom;
        }
        public static void SetDefaultFontDateTimePicker(RadDateTimePicker rad_DateTime, DateTime defalut_DateValues, Font _font)
        {
            // DateTime defalut_DateMax
            rad_DateTime.Format = DateTimePickerFormat.Custom;
            rad_DateTime.Culture = new System.Globalization.CultureInfo("en-US");
            rad_DateTime.CustomFormat = "dd/MM/yyyy";
            rad_DateTime.Font = _font;
            //rad_DateTime.ForeColor = Color.Blue;
            rad_DateTime.Value = defalut_DateValues;
            //rad_DateTime.MaxDate = defalut_DateMax;

            RadDateTimePickerCalendar calendarBehavior = rad_DateTime.DateTimePickerElement.GetCurrentBehavior() as RadDateTimePickerCalendar;
            RadCalendar calendar = calendarBehavior.Calendar as RadCalendar;
            RadCalendarElement calendarElement = calendar.CalendarElement as RadCalendarElement;
            calendarElement.CalendarNavigationElement.Font = Connections.FontTahoma10;
            MonthViewElement monthView = calendarBehavior.Calendar.CalendarElement.CalendarVisualElement as MonthViewElement;
            foreach (RadItem item in monthView.TableElement.Children)
            {
                item.Font = Connections.FontTahoma10;
            }



        }
        public static GridViewDataColumn AppendNewGridViewColumn(ColumnTypes columnType, string columnName, string label, int width, bool visible = true)
        {
            GridViewDataColumn newColumn = null;
            switch (columnType)
            {
                case ColumnTypes.Text:
                    newColumn = new GridViewTextBoxColumn
                    {
                        FieldName = columnName,
                        HeaderText = label,
                        Width = width,
                        IsVisible = visible
                    };
                    break;
                case ColumnTypes.NumericText:
                    newColumn = new GridViewMaskBoxColumn();
                    ((GridViewMaskBoxColumn)newColumn).Mask = "f";
                    newColumn.TextAlignment = ContentAlignment.MiddleLeft;
                    newColumn.FieldName = columnName;
                    newColumn.HeaderText = label;
                    newColumn.Width = width;
                    newColumn.IsVisible = visible;
                    break;
                case ColumnTypes.Decimal:
                    newColumn = new GridViewDecimalColumn
                    {
                        TextAlignment = ContentAlignment.MiddleRight,
                        FieldName = columnName,
                        HeaderText = label,
                        Width = width,
                        IsVisible = visible,
                        FormatString = "{0:N2}"
                    };
                    break;
                case ColumnTypes.DateTime:
                    newColumn = new GridViewDateTimeColumn
                    {
                        FormatString = "{0:MM-dd-yyyy}",
                        TextAlignment = ContentAlignment.MiddleRight,
                        FieldName = columnName,
                        HeaderText = label,
                        Width = width,
                        IsVisible = visible
                    };
                    break;
                case ColumnTypes.Image:
                    newColumn = new GridViewImageColumn();
                    ((GridViewImageColumn)newColumn).ImageLayout = ImageLayout.Center;
                    newColumn.Width = width;
                    newColumn.FieldName = columnName;
                    newColumn.HeaderText = label;
                    newColumn.IsVisible = visible;
                    break;
                case ColumnTypes.Command:
                    newColumn = new GridViewCommandColumn();
                    ((GridViewCommandColumn)newColumn).TextAlignment = ContentAlignment.MiddleCenter;
                    newColumn.FieldName = columnName;
                    newColumn.HeaderText = label;
                    newColumn.Width = width;
                    newColumn.IsVisible = visible;
                    break;
                case ColumnTypes.CheckBox:
                    newColumn = new GridViewCheckBoxColumn
                    {
                        Width = width,
                        FieldName = columnName,
                        HeaderText = label,
                        IsVisible = visible
                    };
                    //newColumn.DataType = typeof(int);
                    //((GridViewCheckBoxColumn)newColumn).EnableHeaderCheckBox = true;
                    //((GridViewCheckBoxColumn)newColumn).ShouldCheckDataRows = false;
                    //((GridViewCheckBoxColumn)newColumn).EditMode = EditMode.OnValueChange;
                    //newColumn.ReadOnly = true;
                    break;
            }
            return newColumn;
        }
        public static void SetDefaultRadGridView(RadGridView grid)
        {
            //grid.Dock = DockStyle.Fill;
            grid.TableElement.RowHeight = 35;
            grid.TableElement.RowHeaderColumnWidth = 40;

            grid.TableElement.Font = new Font("Tahoma", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(000)));

            grid.AutoGenerateColumns = false; //สร้างคอลัมเอง
            grid.ReadOnly = true;
            grid.ShowGroupPanel = false;//ปิดแสดงส่วนของการ group ด้านบนสุด

            grid.EnableAlternatingRowColor = true;//แถวสลับสี

            grid.MasterTemplate.EnableFiltering = true;//การกรองที่หัวกริด
            grid.MasterTemplate.ShowFilterCellOperatorText = false;//แสดงข้อความการค้นหาว่าตามอะไรที่หัสฟิลเลย 

            grid.MasterTemplate.EnableGrouping = true;//แสดงการ Group data (click ขวา)
            grid.MasterTemplate.ShowGroupedColumns = true;//คอลัมที่ Group ให้แสดงด้วย
            grid.MasterTemplate.AutoExpandGroups = true;//แสดงข้อมูลที่ group ทั้งหมด

            grid.MasterTemplate.SelectionMode = GridViewSelectionMode.CellSelect;
            grid.MasterTemplate.ShowHeaderCellButtons = false;//แสดงข้อมูลบนหัวเซลล์ให้ Filter
        }
    }
}
