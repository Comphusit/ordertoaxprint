﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace OrderToAx.Order.Controllers
{
    class ConnectionsDB
    {
      
        internal static DataTable GetSelectSQL(string _sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection sqlConnection = new SqlConnection(Connections.ConnMain);
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adp = new SqlDataAdapter();
                sqlConnection.Open();
                cmd = new SqlCommand(_sql, sqlConnection);
                adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                sqlConnection.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
      
        internal static string ExecuteSQL_Main(string _sql)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(Connections.ConnMain);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    sqlCommand.CommandText = _sql;
                    sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }
      
        internal static string ExecuteSQL_ArrayMain(ArrayList sqlArray, string conn)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(conn);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    for (int i = 0; i < sqlArray.Count; i++)
                    {
                        sqlCommand.CommandText = Convert.ToString(sqlArray[i]);
                        sqlCommand.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }
       
        public static string ExecuteSQL_Array2nd(ArrayList str1, string _ServerName1, ArrayList str2, string _ServerName2)
        {
            //1
            SqlConnection sqlConnection1 = new SqlConnection(_ServerName1);
            if (sqlConnection1.State == ConnectionState.Closed)
            {
                try
                {
                    sqlConnection1.Open();
                }
                catch (Exception)
                {
                    return "ระบบ Server " + Environment.NewLine + _ServerName1 + Environment.NewLine + @" มีปัญหาไม่สามารถส่งข้อมูลได้";
                }
            }

            SqlCommand sqlCommand1 = sqlConnection1.CreateCommand();
            SqlTransaction transaction1 = sqlConnection1.BeginTransaction("SampleTransaction");
            sqlCommand1.Connection = sqlConnection1;
            sqlCommand1.Transaction = transaction1;
            //2
            SqlConnection sqlConnection2 = new SqlConnection(_ServerName2);
            if (sqlConnection2.State == ConnectionState.Closed)
            {
                try
                {
                    sqlConnection2.Open();
                }
                catch (Exception)
                {
                    return "ระบบ Server " + Environment.NewLine + _ServerName2 + Environment.NewLine + @" มีปัญหาไม่สามารถส่งข้อมูลได้";
                }
            }
            // SqlCommand sqlCommandAX = new SqlCommand();
            SqlCommand sqlCommand2 = sqlConnection2.CreateCommand();
            SqlTransaction transaction2 = sqlConnection2.BeginTransaction("SampleTransaction");
            sqlCommand2.Connection = sqlConnection2;
            sqlCommand2.Transaction = transaction2;

            try
            {
                for (int i = 0; i < str1.Count; i++)
                {
                    sqlCommand1.CommandText = Convert.ToString(str1[i]);
                    sqlCommand1.CommandTimeout = 60;
                    sqlCommand1.ExecuteNonQuery();
                }

                for (int j = 0; j < str2.Count; j++)
                {
                    sqlCommand2.CommandText = Convert.ToString(str2[j]);
                    sqlCommand2.CommandTimeout = 60;
                    sqlCommand2.ExecuteNonQuery();
                }

                transaction1.Commit();
                transaction2.Commit();
                sqlConnection1.Close();
                sqlConnection2.Close();
                return "";
            }
            catch (Exception ex)
            {
                transaction1.Rollback();
                transaction2.Rollback();
                return "ไม่สามารถบันทึกข้อมูลเข้าระบบได้ ลองใหม่อีกครั้ง." + Environment.NewLine + ex.Message;
            }
        }
        
    }
}
