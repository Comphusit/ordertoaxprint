﻿using System.Drawing;

namespace OrderToAx.Order.Controllers
{
    public class Connections
    {
        public static string ConnMain = "Data Source = 192.168.100.24;Initial Catalog=SHOP24HRS;User ID=sa;Password=Ca999999";  //VM119SRV_SHOP24HRS
        public static string ConMainAX = "Data Source = 192.168.100.58;Initial Catalog=AX50SP1_SPC;User ID=shop24;Password=Ca999999"; //SPC708SRV_AX50SP1_SPC

        public static string SystemComName = "";
        public static string SystemDept = "";
        
        public static string AppName = "OrderToAxPrint";
        public static string PrintTag = "";
        public static string PrintBill = "";
        public static string WareHouseId = "";
        public static string WareHouseName = "";

        public static string SystemVersion = "4";//Version

        public static Font FontTahoma10 = new Font("Tahoma", 10f, FontStyle.Regular);
        public static Font FontTahomaBold10 = new Font("Tahoma", 10f, FontStyle.Bold);
        public static Font FontTahoma12 = new Font("Tahoma", 12.0f, FontStyle.Regular);
        public static Font FontTahomaBold12 = new Font("Tahoma", 12.0f, FontStyle.Bold);

    }
}
