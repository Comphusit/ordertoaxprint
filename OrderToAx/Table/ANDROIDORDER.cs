﻿namespace OrderToAx.Order.Table
{
    class ANDROIDORDER
    {
        public int SEQNO { get; set; } = 0;
        public string ITEMDIM { get; set; }
        public string ITEMID { get; set; }
        public string ITEMBARCODE { get; set; }
        public string NAME { get; set; }
        public double QTY { get; set; } = 0.0;
        public string UNITID { get; set; }
        public double PRICE { get; set; } = 0.0;
        public string WHOAPV { get; set; }
        public string DATE { get; set; }
        public string BRANCH { get; set; }
        public int SPC_SALESPRICETYPE { get; set; } = 0;
    }
}
