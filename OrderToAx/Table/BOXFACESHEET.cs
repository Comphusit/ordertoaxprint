﻿namespace OrderToAx.Order.Table
{
    class BOXTRANS
    {
        public int ROW { get; set; }
        public string REFBOXTRANS { get; set; }
        public string RECID { get; set; }
        public string NAME { get; set; }
        public string BOXTYPE { get; set; }
        public string BOXQTY { get; set; }
        public string LINEWEIGHT { get; set; }
        public string LINECAPACITY { get; set; }
        public string ITEMBARCODE { get; set; }
        public string RECVERSION { get; set; }
        public string REFPALLETTRANS { get; set; }                           
    }
    class BOXFACESHEET
    {
        public int ROW { get; set; }
        public string INVENTLOCATIONIDTO { get; set; }
        public string LOCATIONNAMETO { get; set; }
        public string ROUTENAME { get; set; }
        public string BOXGROUP { get; set; }
        public string REFBOXTRANS { get; set; }
        public string SHIPPINGDATEREQUESTED { get; set; }
        public string QTY { get; set; }
        public string NUMBERLABEL { get; set; }
        public string DLVNAME { get; set; }
        public string BOXNAME { get; set; }
        public string BOXTYPE { get; set; }
        public string RECVERSION { get; set; }
        public string RECID { get; set; }
    }
    class SPC_BOXFACESHEET
    {
        public string SHIPDATE { get; set; }
        public string BOXTYPE  { get; set; }
        public string BOXGROUP { get; set; }
        public string DOCUMENTNUM { get; set; }
        public string NUMBERLABEL { get; set; }
        public string LOCATIONNAMETO { get; set; }
        public string ROUTENAME { get; set; }
        public string SETDATETIME { get; set; }
    }
    class DOCUMENTMRTO
    {
        public string DOCNO { get; set; }
        public string DATEDOC { get; set; }
        public string BRANCHNAME { get; set; }
        public int COUNTBOX { get; set; }
        public int BOXPRINT { get; set; }
        public string ROUTEID { get; set; }
        public string WHOUP { get; set; }
    }
    class BOXGROUP
    {
        public int BOX { get; set; }
        public string BOXNAME { get; set; }
    }
    class COUNTBOX
    {
        public string VOUCHERID { get; set; }
        public int BOX { get; set; }
    }
    class POSTOBOXFACESHEET
    {
        public string SHIPDATE { get; set; }
        public string BOXGROUP { get; set; }
        public string DOCUMENTNUM { get; set; }
        public string NUMBERLABEL { get; set; }
        public string LOCATIONNAMETO { get; set; }
        public string ROUTENAME { get; set; }
        public string SETDATETIME { get; set; }
    }
}
