﻿namespace OrderToAx.Order.Table
{
    class INVENTTRANSFER
    {
        public string VOUCHERID { get; set; } = "";
        public string DOCNO { get; set; } = "";
        public string DATEDOC { get; set; } = "";
        public string TIMEDOCNO { get; set; } = "";
        public string BRANCHNAME { get; set; } = "";
        public string ROUTEID { get; set; } = "";
        public string ROUTENAME { get; set; } = "";
        public string STATUS { get; set; } = "";
        public string ITEMTYPE { get; set; } = "";
        public string PRINTTAG { get; set; } = "";
        public int COUNTBOX { get; set; } = 0;
        public int BOXPRINT { get; set; } = 0;
        public string WHOAPV { get; set; } = "";
        public string WHOUP { get; set; } = "";
    }
    class INVENTJOURLINE
    {
        public string SPC_ITEMNAME { get; set; } = "";
        public string TRANSFERID { get; set; } = "";
        public string ITEMID { get; set; } = "";
        public string SPC_ITEMBARCODE { get; set; } = "";
        public double QTYSHIPPED { get; set; }
        public string UNITID { get; set; } = "";
    }
    class BILLTRANS
    {
        public string DOCNO { get; set; }
        public string BRANCHID { get; set; }
        public string ROUTE { get; set; }
        public string DEPT { get; set; }
        public string TYPEBILL { get; set; }
        public string DATEDOC { get; set; }
        public string TIMEDOC { get; set; }
    }
}
