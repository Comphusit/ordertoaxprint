﻿using OrderToAx.Order.Controllers;

namespace OrderToAx.Order.Table
{
    //class CONNECTION
    //{
    //    public string VARIABLENAME { get; set; }
    //    public string SERVERIP { get; set; }
    //    public string CATALOG { get; set; }
    //    public string USERNAME { get; set; }
    //    public string PASSWORD { get; set; }
    //}
    class COLUMNS
    {
        public Property.ColumnTypes TYPE { get; set; }
        public string COLUMNNAME { get; set; }
        public string LABEL { get; set; }
        public int WIDTH { get; set; }
        public bool VISIBLE { get; set; } = true;
    }
    public class Item
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Item(int id, string description)
        {
            this.Id = id;
            this.Description = description;
        }
    }
    //public class Dept
    //{
    //    public string DeptId { get; set; }
    //    public Dept(string comName)
    //    {
    //        string sql = string.Format(@"
    //        SELECT  DEPT_ID
    //        FROM    SHOP_BRANCH_PERMISSION  WITH(NOLOCK) INNER JOIN
    //                SHOP_CONFIGBRANCH_GenaralDetail WITH(NOLOCK)ON SHOP_BRANCH_PERMISSION.SHOW_ID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
    //                AND SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID = 'A05'
    //                AND SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '15'
    //                AND STAUSE = '1'
    //        WHERE   COM_NAME = '{0}'
    //                AND SHOP_BRANCH_PERMISSION.SHOW_ID = 'A05'"
    //                , comName);

    //        DataTable dt = ConnectionsDB.GetSelectSQL(sql);
    //        this.DeptId = dt.Rows.Count > 0 ? dt.Rows[0]["DEPT_ID"].ToString(): "";
    //    }
    //}
}
